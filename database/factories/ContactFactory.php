<?php

use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'mobile'=>$faker->phoneNumber,
        'address'=>$faker->sentence,
        'email'=>$faker->email,
        'facebook'=>$faker->email,
        'text'=>$faker->sentence
    ];
});
