<?php

use Faker\Generator as Faker;

$factory->define(App\About::class, function (Faker $faker) {
    return [
        'heading'=>$faker->title,
//        'avatar'=>$faker->avatar,
        'text_heading'=>$faker->title,
        'text'=>$faker->sentence
    ];
});
