<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaRequirmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_requirments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('visa_id');
            $table->unsignedInteger('requirment_id');
            $table->timestamps();
            $table->foreign('visa_id')->references('id')->on('visas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('requirment_id')->references('id')->on('requirments')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_requirments');
    }
}
