<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('heading');
            $table->string('cover_url');
            $table->string('cover_name')->nullable();
            $table->string('price');
            $table->mediumText('description');
            $table->longText('pkg_includes');
            $table->longText('pkg_exclusive');
            $table->text('package_title');
            $table->string('departure_country');
            $table->string('departure_city');
            $table->string('min_allowed');
            $table->string('ticket');
            $table->string('visa');
            $table->string('insurance');
            $table->string('stay');
            $table->string('hotel');
            $table->string('company');
            $table->string('rate_mentioned');
            $table->date('valid_till');
            $table->date('posted_on');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
