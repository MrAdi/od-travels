<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $guarded = [];
    public function images(){
        return $this->hasMany(Tour_images::class,'tours_id','id');
    }
}
