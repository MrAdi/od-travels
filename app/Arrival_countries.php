<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrival_countries extends Model
{
    protected $guarded = [];

    public function country(){
        return $this->belongsTo('App\Country');
    }
    public function countries(){
        return $this->belongsTo('App\Country','country_id','id');
    }
}

