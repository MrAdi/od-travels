<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requirments extends Model
{
    protected $table = 'requirments';
    protected $fillable = [
        'name' ,
        'status',
    ];
    public function requirments(){
        return $this->belongsToMany(Visa::class,'visa_requirment','requirments_id','visa_id');
    }
//    public function req_name(){
//        return $this->belongsTo('App/')
//}
}
