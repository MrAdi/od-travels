<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home_slider extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'avatar'
    ];
}
