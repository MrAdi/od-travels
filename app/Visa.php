<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Country;

class Visa extends Model
{
    protected $guarded = [];

    public function requirments(){
        return $this->belongsToMany(Requirments::class,'visa_requirments','visa_id','requirment_id');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }
    public function countries(){
        return $this->belongsTo('App\Country','country_id','id');
    }
    public function country2(){
        return $this->belongsTo('App\Country','country_id2','id');
    }

}
