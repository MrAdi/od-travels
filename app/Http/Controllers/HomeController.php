<?php

namespace App\Http\Controllers;

use App\About;
use App\Arrival_countries;
use App\Contact;
use App\Country;
use App\Cta_slider;
use App\Home;
use App\Home_slider;
use App\insaurance;
use App\News;
use App\Offer;
use App\Requirments;
use App\Testimonial;
use App\Tour;
use App\Tour_images;
use App\Tour_slider;
use App\Tour_video;
use App\Umrah;
use App\Visa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index()
    {

//        return view('index');
//        $abouts=About::orderBy('id','dec')->limit(4)->get();

        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
//        orderBy('id','dec')->limit(4)->get();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();

        return view('index',compact('contacts','countrys','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));
    }
    public function visa($id)
    {

        $visa_detail=Visa::where('id',$id)->first();
        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $offers=Offer::all();
        $visa_requirments=DB::table('visa_requirments')->where('visa_id',$id)->get();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();

        return view('visa',compact('contacts','countrys','home_sliders','news','offers','requirments','visa_requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas','visa_detail'));


    }

    public function visa_category()
    {

        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::first();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();

        return view('visa_category',compact('contacts','countrys','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }

    public function tour()
    {
        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::first();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::first();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('tour_packages',compact('contacts','countrys','tour_image','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }

    public function tour_detail($id)
    {

        $tour_detail=Tour::where('id',$id)->first();
        $tour_image=DB::table('tour_images')->where('tours_id',$id)->get();
        $countrys=Country::all();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $contacts=Contact::first();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('tour_packages_detail',compact('contacts','tour_image','countrys','tour_detail','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }

    public function domastic_tour()
    {
        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::first();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::first();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('domastic_tour_packages',compact('contacts','countrys','tour_image','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }

    public function domastic_tour_detail()
    {

//        $tour_detail=Tour::where('id',$id)->first();
//        $tour_image=DB::table('tour_images')->where('tours_id',$id)->get();
//        $countrys=Country::all();
//        $home_sliders=Home_slider::all();
//        $news=News::all();
        $contacts=Contact::first();
//        $offers=Offer::all();
//        $requirments=Requirments::all();
//        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
//        $tours=Tour::all();
//        $tour_images=Tour_images::all();
//        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
//        $visas=Visa::all();
//        return view('domastic_tour_packages_detail',compact('contacts','tour_image','countrys','tour_detail','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));

return view('domastic_tour_packages_detail',compact('tour_videos','contacts'));
    }


    public function about_us()
    {

        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('about_us',compact('contacts','countrys','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }

    public function contact_us()
    {

        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('contact_us',compact('contacts','countrys','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }
    public function insaurance()
    {
        $insaurance=insaurance::firstOrNew([]);

        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('insaurance',compact('contacts','insaurance','countrys','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }
    public function arrival_countries()
    {

        $arrival_countries=Arrival_countries::orderBy('id','dec')->get();
        $countrys=Country::all();
        $contacts=Contact::first();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('arrival_countries',compact('contacts','arrival_countries','countrys','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }
    public function umraah()
    {
        $umrahs=Umrah::orderBy('id','dec')->get();

        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::first();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();
        return view('umraah',compact('contacts','umrahs','countrys','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));


    }
//    function send(Request $request)
//    {
//        $this->validate($request, [
//            'name'     =>  'required',
//            'email'  =>  'required|email',
//            'phone' =>  'required',
//            'subject' =>  'required',
//            'message' =>  'required'
//        ]);
//
//        $data = array(
//            'name'      =>  $request->name,
//            'email'      =>  $request->email,
//            'phone'      =>  $request->phone,
//            'subject'      =>  $request->subject,
//            'message'   =>   $request->message
//        );
//
//        Mail::to('zubairshafiq868@gmail.com')->send(new SendMail($data));
//        return back()->with('success', 'Thanks for contacting us!');
//
//    }


    public function searchtours(Request $request)
    {
        $contacts=Contact::first();
        $countrys=Country::all();
        $home_sliders=Home_slider::all();
        $news=News::all();
        $offers=Offer::all();
        $requirments=Requirments::all();
        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
        $tours=Tour::all();
//        orderBy('id','dec')->limit(4)->get();
        $tour_images=Tour_images::all();
        $tour_sliders=Tour_slider::all();
        $tour_videos=Tour_video::all();
        $visas=Visa::all();


        if ($request->isMethod('post')){
            $data = $request->all();
//            dd($data);
            $search_tour = $data['heading'];
            $search_price = $data['price'];
//            dd($search_price,$search_tour);
            $tours_search = Tour::where('heading',$search_tour);
            if (!is_null($search_price)){
                $tours_search = $tours_search->where('price','<',(int)$search_price);
            }
//            dd($tours_search->toSql());
            $tours_search = $tours_search->get();
//            return view('index',compact('tours_search'));
            return view('index',compact('contacts','countrys','tours_search','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));




        }

//        $key=\Request::get('adults');
//        $name=Tour::where('heading' , 'LIKE' ,'%' .$key.'%')->paginate(3);
//        return back();

    }

//    public function searchcountries(Request $request)
//    {
//        $contacts=Contact::first();
//        $countrys=Country::all();
//        $home_sliders=Home_slider::all();
//        $news=News::all();
//        $offers=Offer::all();
//        $requirments=Requirments::all();
//        $testimonials=Testimonial::orderBy('id','dec')->limit(4)->get();
//        $tours=Tour::all();
////        orderBy('id','dec')->limit(4)->get();
//        $tour_images=Tour_images::all();
//        $tour_sliders=Tour_slider::all();
//        $tour_videos=Tour_video::all();
//        $visas=Visa::all();
//
//
//        if ($request->isMethod('post')){
//            $data = $request->all();
////            dd($data);
//            $search_visa = $data['heading'];
//            $search_price = $data['price'];
////            dd($search_price,$search_tour);
//            $visa_search = Visa::where('heading',$search_visa);
//            if (!is_null($search_price)){
//                $tours_search = $tours_search->where('price','<',(int)$search_price);
//            }
////            dd($tours_search->toSql());
//            $tours_search = $tours_search->get();
////            return view('index',compact('tours_search'));
//            return view('index',compact('contacts','countrys','tours_search','home_sliders','news','offers','requirments','testimonials','tours','tour_images','tour_sliders','tour_videos','visas'));
//
//
//
//
//        }
//
//        $q = Input::get ( 'q' );
//        $countries = Country::where('name','LIKE','%'.$q.'%')->get();
//        if(count($countries) > 0)
//            return view('welcome')->withDetails($countries)->withQuery ( $q );
//        else return view ('welcome')->withMessage('No Details found. Try to search again !');
//
//
////        $key=\Request::get('adults');
////        $name=Tour::where('heading' , 'LIKE' ,'%' .$key.'%')->paginate(3);
////        return back();
//
//    }

    public function searchcountries(Request $request,$id)
    {


        $visa_detail = Visa::where('id', $id)->first();
        $contacts = Contact::first();
        $countrys = Country::all();
        $home_sliders = Home_slider::all();
        $news = News::all();
        $offers = Offer::all();
        $visa_requirments = DB::table('visa_requirments')->where('visa_id', $id)->get();
        $requirments = Requirments::all();
        $testimonials = Testimonial::orderBy('id', 'dec')->limit(4)->get();
        $tours = Tour::all();
        $tour_images = Tour_images::all();
        $tour_sliders = Tour_slider::all();
        $tour_videos = Tour_video::all();
        $visas = Visa::all();

        if ($request->isMethod('post')) {
//            $q = $request->all();
            $q = Input::get('q');
            $countries = Country::where('name', 'LIKE', '%' . $q . '%')->get();
            if (count($countries) > 0)
                return view('visa', compact('contacts', 'countrys', 'home_sliders', 'news', 'offers', 'requirments', 'visa_requirments', 'testimonials', 'tours', 'tour_images', 'tour_sliders', 'tour_videos', 'visas', 'visa_detail'))->withDetails($countries)->withQuery($q);
            else return view('visa', compact('contacts', 'countrys', 'home_sliders', 'news', 'offers', 'requirments', 'visa_requirments', 'testimonials', 'tours', 'tour_images', 'tour_sliders', 'tour_videos', 'visas', 'visa_detail'))->withMessage('No Details found. Try to search again !');

        }


    }
}
