<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function uploadFile($path,$file){
        $pathSections = explode('/',$path);
        $count = 0; $tempPath = '';
        while ($count != count($pathSections)){
            if($count < count($pathSections)){
                if($count > 0){
                    $tempPath .= '/';
                }
                $tempPath .= $pathSections[$count];

                if(!\File::isDirectory(public_path($tempPath)))
                    \File::makeDirectory(public_path($tempPath));

                ++$count;
            }
        }

        $fileExtension = $file->getClientOriginalExtension();
        $fileName = time().'_'.rand(100,999).'.'.$fileExtension;
        $file->move(public_path($path), $fileName);

        return [
            'file_name'=>$fileName,
            'file_url'=>$path.'/'.$fileName
        ];
    }

    public function removeFile($path){
        if(\File::exists(public_path($path)))
            \File::delete(public_path($path));
    }
}
