<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmailController extends Controller
{
    function index()
    {
        $contacts=Contact::first();
        return view('contact_us',compact('contacts'));
    }
    function send(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'email'  =>  'required|email',
            'phone' =>  'required',
            'subject' =>  'required',
            'message' =>  'required'
        ]);
//        dd($request);

        $data = array(
            'name'      =>  $request->name,
            'email'      =>  $request->email,
            'phone'      =>  $request->phone,
            'subject'      =>  $request->subject,
            'message'   =>   $request->message
        );

        Mail::to('zubairshafiq868@gmail.com')->send(new SendMail($data));
        return back()->with('success', 'Thanks for contacting us!');

    }
}
