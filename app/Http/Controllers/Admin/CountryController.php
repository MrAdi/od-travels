<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=Country::all();
        return view('admin.pages.country.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $adis=Country::all();
        return view('admin.pages.country.create',compact('adis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'avatar'=>'required',


        ]);
        if ($request->file('avatar')){
            $file = $this->uploadFile('uploads/country/',$request->file('avatar'));
        }
        Country::create([
            'name'=>$request->name,
            'avatar'=>$file['file_url'],

        ]);
        return redirect(route('admin.country.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        return view('admin.pages.country.show',compact('country'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        $adis=Country::all();
        return view('admin.pages.country.edit',compact('country','adis'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        if ($request->file('avatar')){
            $this->removeFile($country->avatar);
            $file = $this->uploadFile('uploads/country/',$request->file('avatar'));
            $country->avatar=$file['file_url'];
        }
        $country->name=$request->name;

        $country->save();

        return redirect(route('admin.country.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        $country->delete();
        return redirect(route('admin.country.index'));
    }
}
