<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Requirments;
use App\Visa;
use Illuminate\Http\Request;

class RequirmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=Requirments::orderBy('id','dec')->get();
        return view('admin.pages.requirements.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $visas=Visa::all();
        return view('admin.pages.requirements.create',compact('visas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=>'required',
            'status'=>'required',

        ]);
//        dd($request->status);
        Requirments::create([
            'name'=>$request->name,
            'status'=>$request->status,

        ]);
        return redirect(route('admin.requirments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requirments  $requirments
     * @return \Illuminate\Http\Response
     */
    public function show(Requirments $requirments)
    {
        return view('admin.pages.requirements.show',compact('requirments'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requirments  $requirments
     * @return \Illuminate\Http\Response
     */
    public function edit(Requirments $requirment)
    {
        $visas=Visa::all();
        return view('admin.pages.requirements.edit',compact('requirment','visas'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requirments  $requirments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requirments $requirment)
    {
        $requirment->name=$request->name;
        $requirment->status=$request->status;
        $requirment->save();
        return redirect(route('admin.requirments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requirments  $requirments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requirments $requirment)
    {
        $requirment->delete();
        return redirect(route('admin.requirments.index'));
    }
}
