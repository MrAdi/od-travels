<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\insaurance;
use Illuminate\Http\Request;

class InsauranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=insaurance::orderBy('id','dec')->get();
        return view('admin.pages.insaurance.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.insaurance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'picture'=>'required',

        ]);
        if ($request->file('picture')){
            $file = $this->uploadFile('uploads/insaurance/',$request->file('picture'));
        }
        insaurance::create([

            'picture'=>$file['file_url'],

        ]);

        return redirect(route('admin.insaurance.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\insaurance  $insaurance
     * @return \Illuminate\Http\Response
     */
    public function show(insaurance $insaurance)
    {
        return view('admin.pages.insaurance.show',compact('insaurance'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\insaurance  $insaurance
     * @return \Illuminate\Http\Response
     */
    public function edit(insaurance $insaurance)
    {
        return view('admin.pages.insaurance.edit',compact('insaurance'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\insaurance  $insaurance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, insaurance $insaurance)
    {
        if ($request->file('picture')){
            $this->removeFile($insaurance->picture);
            $file = $this->uploadFile('uploads/insaurance/',$request->file('picture'));
            $insaurance->picture=$file['file_url'];
        }

        $insaurance->save();
        return redirect(route('admin.insaurance.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\insaurance  $insaurance
     * @return \Illuminate\Http\Response
     */
    public function destroy(insaurance $insaurance)
    {
        $insaurance->delete();
        return redirect(route('admin.insaurance.index'));
    }
}
