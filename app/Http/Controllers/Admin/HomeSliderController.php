<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Home_slider;
use Illuminate\Http\Request;

class HomeSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=Home_slider::orderBy('id','dec')->get();
        return view('admin.pages.home_slider.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.home_slider.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
//        $file=$request->file('thumbnail');
//        $destinationPath = 'assets/uploads/about-pics';
//        $filename = $file->getClientOriginalName();
//        $filename = time() . $filename;
//
//        $file->move($destinationPath, $filename);
//        $request->merge(['avatar' => $filename]);
//
//        Home_slider::create($request->all());

        $this->validate($request,[
            'avatar'=>'required',

        ]);
        if ($request->file('avatar')){
            $file = $this->uploadFile('uploads/home_slider/',$request->file('avatar'));
        }
        Home_slider::create([

            'avatar'=>$file['file_url'],



        ]);

        return redirect(route('admin.home_slider.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Home_slider  $home_slider
     * @return \Illuminate\Http\Response
     */
    public function show(Home_slider $home_slider)
    {
        return view('admin.pages.home_slider.show',compact('home_slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Home_slider  $home_slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Home_slider $home_slider)
    {
//        dd($home_slider->all());
        return view('admin.pages.home_slider.edit',compact('home_slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Home_slider  $home_slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Home_slider $home_slider)
    {
//        dd($home_slider->all());

        if ($request->file('avatar')){
            $this->removeFile($home_slider->avatar);
            $file = $this->uploadFile('uploads/home_slider/',$request->file('avatar'));
            $home_slider->avatar=$file['file_url'];
        }

        $home_slider->save();
        return redirect(route('admin.home_slider.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Home_slider  $home_slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Home_slider $home_slider)
    {
        $home_slider->delete();
        return redirect(route('admin.home_slider.index'));
    }
}
