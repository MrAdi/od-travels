<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=News::orderBy('id','dec')->get();
        return view('admin.pages.news.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'text'=>'required',
            'image'=>'required',
            'title'=>'required',


        ]);
        if ($request->file('image')){
            $file = $this->uploadFile('uploads/news/',$request->file('image'));
        }
        News::create([

            'title'=>$request->title,
            'text'=>$request->text,
            'image'=>$file['file_url'],


            ]);
        return redirect(route('admin.news.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        return view('admin.pages.news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
//        dd('kun');
        return view('admin.pages.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {

        if ($request->file('image')){
            $this->removeFile($news->image);
            $file = $this->uploadFile('uploads/news/',$request->file('image'));
            $news->image=$file['file_url'];
        }

        $news->text=$request->text;
        $news->title=$request->title;
        $news->save();
        return redirect(route('admin.news.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect(route('admin.news.index'));
    }
}
