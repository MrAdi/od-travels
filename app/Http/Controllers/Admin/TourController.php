<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Tour;
use Illuminate\Http\Request;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=Tour::orderBy('id','dec')->get();
        return view('admin.pages.tours.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.tours.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'heading'=>'required',
            'cover_url'=>'required',
//            'cover_name'=>'required',
            'price'=>'required',
            'description'=>'required',
            'pkg_includes'=>'required',
            'pkg_exclusive'=>'required',
            'package_title'=>'required',
            'departure_country'=>'required',
            'departure_city'=>'required',
            'min_allowed'=>'required',
            'ticket'=>'required',
            'visa'=>'required',
            'insurance'=>'required',
            'stay'=>'required',
            'hotel'=>'required',
            'company'=>'required',
            'rate_mentioned'=>'required',
            'valid_till'=>'required',
            'posted_on'=>'required',


        ]);

        $date1=date('Y-m-d',strtotime($request->valid_till));
        $date2=date('Y-m-d',strtotime($request->posted_on));

        if ($request->file('cover_url')){
            $file = $this->uploadFile('uploads/tours/',$request->file('cover_url'));
        }
        Tour::create([
            'heading'=>$request->heading,
            'cover_url'=>$file['file_url'],
            'price'=>$request->price,
            'description'=>$request->description,
            'pkg_includes'=>$request->pkg_includes,
            'pkg_exclusive'=>$request->pkg_exclusive,
            'package_title'=>$request->package_title,
            'departure_country'=>$request->departure_country,
            'departure_city'=>$request->departure_city,
            'min_allowed'=>$request->min_allowed,
            'ticket'=>$request->ticket,
            'visa'=>$request->visa,
            'insurance'=>$request->insurance,
            'stay'=>$request->stay,
            'hotel'=>$request->hotel,
            'company'=>$request->company,
            'rate_mentioned'=>$request->rate_mentioned,
            'valid_till'=>$date1,
            'posted_on'=>$date2,
        ]);
        return redirect(route('admin.tours.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function show(Tour $tour)
    {
        return view('admin.pages.tours.show',compact('tour'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour $tour)
    {
        return view('admin.pages.tours.edit',compact('tour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour $tour)
    {
        if ($request->file('cover_url')){
            $this->removeFile($tour->cover_url);
            $file = $this->uploadFile('uploads/tours/',$request->file('cover_url'));
            $tour->cover_url=$file['file_url'];
        }

        $tour->heading=$request->heading;
        $tour->price=$request->price;
        $tour->description=$request->description;
        $tour->pkg_includes=$request->pkg_includes;
        $tour->pkg_exclusive=$request->pkg_exclusive;
        $tour->package_title=$request->package_title;
        $tour->departure_country=$request->departure_country;
        $tour->departure_city=$request->departure_city;
        $tour->min_allowed=$request->min_allowed;
        $tour->ticket=$request->ticket;
        $tour->visa=$request->visa;
        $tour->insurance=$request->insurance;
        $tour->stay=$request->stay;
        $tour->hotel=$request->hotel;
        $tour->company=$request->company;
        $tour->rate_mentioned=$request->rate_mentioned;
        $tour->valid_till= date('Y-m-d',strtotime($request->valid_till));
        $tour->posted_on= date('Y-m-d',strtotime($request->posted_on));
        $tour->save();
        return redirect(route('admin.tours.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour $tour)
    {
        $tour->delete();
        return redirect(route('admin.tours.index'));
    }





}
