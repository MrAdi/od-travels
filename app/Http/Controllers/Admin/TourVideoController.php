<?php

namespace App\Http\Controllers\Admin;

use App\Tour_video;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TourVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=Tour_video::orderBy('id','dec')->get();
        return view('admin.pages.tours_videos.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.tours_videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'url'=>'required',



        ]);

        Tour_video::create([
            'url'=>$request->url,

        ]);
        return redirect(route('admin.tours_videos.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tour_video  $tour_video
     * @return \Illuminate\Http\Response
     */
    public function show(Tour_video $tour_video)
    {
        return view('admin.pages.tours_videos.show',compact('tour_video'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tour_video  $tour_video
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour_video $tours_video)
    {
        return view('admin.pages.tours_videos.edit',compact('tours_video'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tour_video  $tour_video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour_video $tours_video)
    {
        $tours_video->url=$request->url;
        $tours_video->save();

        return redirect(route('admin.tours_videos.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tour_video  $tour_video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour_video $tours_video)
    {
        $tours_video->delete();
        return redirect(route('admin.tours_videos.index'));
    }
}
