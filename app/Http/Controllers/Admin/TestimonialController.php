<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=Testimonial::orderBy('id','dec')->get();
        return view('admin.pages.testimonials.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.testimonials.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'title'=>'required',
            'avatar'=>'required',
            'text'=>'required',
            'date'=>'required',

        ]);
        $date=date('Y-m-d',strtotime($request->date));

        if ($request->file('avatar')){
            $file = $this->uploadFile('uploads/testimonials/',$request->file('avatar'));
        }
        Testimonial::create([
            'name'=>$request->name,
            'title'=>$request->title,
            'avatar'=>$file['file_url'],
            'text'=>$request->text,
            'date'=> $date,
        ]);
        return redirect(route('admin.testimonials.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        return view('admin.pages.testimonials.show',compact('testimonial'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('admin.pages.testimonials.edit',compact('testimonial'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {


        if ($request->file('avatar')){
            $this->removeFile($testimonial->avatar);
            $file = $this->uploadFile('uploads/testimonials/',$request->file('avatar'));
            $testimonial->avatar=$file['file_url'];
        }
        $testimonial->name=$request->name;
        $testimonial->title=$request->title;
        $testimonial->text=$request->text;
        $testimonial->date= date('Y-m-d',strtotime($request->date));
        $testimonial->save();

        return redirect(route('admin.testimonials.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        $testimonial->delete();
        return redirect(route('admin.testimonials.index'));
    }
}
