<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Tour;
use App\Tour_images;
use Illuminate\Http\Request;

class TourImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours = Tour::all();
        $adis = Tour_images::orderBy('id', 'dec')->get();
        return view('admin.pages.tours_images.index', compact('adis', 'tours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tours = Tour::all();
//        dd(Tour::all());
        return view('admin.pages.tours_images.create', compact('tours'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'tours_id' => 'required',
            'url' => 'required',


        ]);

        if ($request->url) {
            foreach ($request->url as $image) {

                $file = $this->uploadFile('uploads/tours_images/', $image);

                Tour_images::create([
                    'tours_id' => $request->tours_id,
                    'url' => $file['file_url'],
                ]);
            }
        }

        return redirect(route('admin.tours_images.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tour_images $tour_images
     * @return \Illuminate\Http\Response
     */
    public function show(Tour_images $tour_images)
    {
        return view('admin.pages.tours_images.show', compact('tour_images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tour_images $tour_images
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour_images $tours_image)
    {
        $tours = Tour::all();
//        dd($tours_image);
        return view('admin.pages.tours_images.edit', compact('tours_image', 'tours'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tour_images $tour_images
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour_images $tours_image)
    {
        if ($request->file('url')){
            $this->removeFile($tours_image->url);
            $file = $this->uploadFile('uploads/tours_images/',$request->file('url'));
            $tours_image->url=$file['file_url'];
        }


//        dd('here');
        $tours_image->tours_id = $request->tours_id;
        $tours_image->save();

        return redirect(route('admin.tours_images.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tour_images $tour_images
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour_images $tours_image)
    {
        $tours_image->delete();
        return redirect(route('admin.tours_images.index'));

    }
}
