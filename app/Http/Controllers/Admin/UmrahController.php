<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Umrah;
use Illuminate\Http\Request;

class UmrahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adis=Umrah::orderBy('id','dec')->get();
        return view('admin.pages.umrah.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.umrah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cover_url'=>'required',
            'name'=>'required',
            'days'=>'required',
            'url'=>'required',
            'hotel'=>'required',
            'visa'=>'required',
            'ticket'=>'required',
            'price'=>'required',



        ]);
        if ($request->file('cover_url')){
            $cover_file = $this->uploadFile('uploads/umrah/',$request->file('cover_url'));
        }
        if ($request->file('url')){
            $image_file = $this->uploadFile('uploads/umrah/',$request->file('url'));
        }
        Umrah::create([
            'cover_url'=>$cover_file['file_url'],
            'name'=>$request->name,
            'days'=>$request->days,
            'url'=>$image_file['file_url'],
            'hotel'=>$request->hotel,
            'visa'=>$request->visa,
            'ticket'=>$request->ticket,
            'price'=>$request->price,
             ]);
        return redirect(route('admin.umrah.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Umrah  $umrah
     * @return \Illuminate\Http\Response
     */
    public function show(Umrah $umrah)
    {
        return view('admin.pages.umrah.show',compact('umrah'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Umrah  $umrah
     * @return \Illuminate\Http\Response
     */
    public function edit(Umrah $umrah)
    {
        return view('admin.pages.umrah.edit',compact('umrah'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Umrah  $umrah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Umrah $umrah)
    {
        if ($request->file('cover_url')){
            $this->removeFile($umrah->cover_url);
            $file = $this->uploadFile('uploads/umrah/',$request->file('cover_url'));
            $umrah->cover_url=$file['file_url'];
        }
        if ($request->file('url')){
            $this->removeFile($umrah->url);
            $file = $this->uploadFile('uploads/umrah/',$request->file('url'));
            $umrah->url=$file['file_url'];
        }

        $umrah->name=$request->name;
        $umrah->days=$request->days;
        $umrah->hotel=$request->hotel;
        $umrah->visa=$request->visa;
        $umrah->ticket=$request->ticket;
        $umrah->price=$request->price;
        $umrah->save();
        return redirect(route('admin.umrah.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Umrah  $umrah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Umrah $umrah)
    {
        $umrah->delete();
        return redirect(route('admin.umrah.index'));
    }
}
