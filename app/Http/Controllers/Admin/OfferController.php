<?php

namespace App\Http\Controllers\Admin;

use App\Offer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $adis=Offer::orderBy('id','dec')->get();
        return view('admin.pages.offers.index',compact('adis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.offers.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'heading'=>'required',
            'avatar'=>'required',
            'price'=>'required',
            'text'=>'required',

        ]);
        if ($request->file('avatar')){
            $file = $this->uploadFile('uploads/offers/',$request->file('avatar'));
        }
        Offer::create([
            'heading'=>$request->heading,
            'avatar'=>$file['file_url'],
            'price'=>$request->price,
            'text'=>$request->text
        ]);
        return redirect(route('admin.offers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        return view('admin.pages.offers.show',compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        return view('admin.pages.offers.edit',compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        if ($request->file('avatar')){
            $this->removeFile($offer->avatar);
            $file = $this->uploadFile('uploads/offers/',$request->file('avatar'));
            $offer->avatar=$file['file_url'];
        }

        $offer->heading=$request->heading;
        $offer->price=$request->price;
        $offer->text=$request->text;
        $offer->save();
        return redirect(route('admin.offers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();
        return redirect(route('admin.offers.index'));
    }
}
