<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Domastic;
use Illuminate\Http\Request;

class DomasticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Domastic  $domastic
     * @return \Illuminate\Http\Response
     */
    public function show(Domastic $domastic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Domastic  $domastic
     * @return \Illuminate\Http\Response
     */
    public function edit(Domastic $domastic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Domastic  $domastic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Domastic $domastic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Domastic  $domastic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Domastic $domastic)
    {
        //
    }
}
