<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Tour;
use App\Tour_slider;
use Illuminate\Http\Request;

class TourSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours=Tour::all();
        $adis=Tour_slider::orderBy('id','dec')->get();
        return view('admin.pages.tours_slider.index',compact('adis','tours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tours=Tour::all();
        return view('admin.pages.tours_slider.create',compact('tours'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $this->validate($request,[
            'tours_id'=>'required',
            'heading'=>'required',
            'text'=>'required',


        ]);


        Tour_slider::create([
            'tours_id'=>$request->tours_id,
            'heading'=>$request->heading,
            'text'=>$request->text
        ]);

        return redirect(route('admin.tours_slider.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tour_slider  $tour_slider
     * @return \Illuminate\Http\Response
     */
    public function show(Tour_slider $tour_slider)
    {
        return view('admin.pages.tours_slider.show',compact('tour_slider'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tour_slider  $tour_slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour_slider $tours_slider)
    {
        $tours=Tour::all();
        return view('admin.pages.tours_slider.edit',compact('tours_slider','tours'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tour_slider  $tour_slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour_slider $tours_slider)
    {

        $tours_slider->tours_id=$request->tours_id;
        $tours_slider->heading=$request->heading;
        $tours_slider->text=$request->text;
        $tours_slider->save();

        return redirect(route('admin.tours_slider.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tour_slider  $tour_slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour_slider $tours_slider)
    {
        $tours_slider->delete();
        return redirect(route('admin.tours_slider.index'));
    }
}
