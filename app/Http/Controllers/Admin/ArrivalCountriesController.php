<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Http\Controllers\Controller;
use App\Arrival_countries;
use Illuminate\Http\Request;

class ArrivalCountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $countrys=Country::all();
        $adis=Arrival_countries::orderBy('id','dec')->get();
        return view('admin.pages.arrival_countries.index',compact('adis','countrys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys=Country::all();
        return view('admin.pages.arrival_countries.create',compact('countrys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'country_id'=>'required',

        ]);

        Arrival_countries::create([

            'country_id'=>$request->country_id,

        ]);

        return redirect(route('admin.arrival_countries.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Arrival_countries  $arrival_countries
     * @return \Illuminate\Http\Response
     */
    public function show(Arrival_countries $arrival_countrie)
    {
        return view('admin.pages.arrival_countries.show',compact('arrival_countrie'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Arrival_countries  $arrival_countries
     * @return \Illuminate\Http\Response
     */
    public function edit(Arrival_countries $arrival_countrie)
    {
        $countrys=Country::all();
        return view('admin.pages.arrival_countries.edit',compact('arrival_countrie','countrys'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Arrival_countries  $arrival_countries
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Arrival_countries $arrival_countrie)
    {

        $arrival_countrie->country_id=$request->country_id;
        $arrival_countrie->save();
        return redirect(route('admin.arrival_countries.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Arrival_countries  $arrival_countries
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Arrival_countries::find($id)->delete();
        return redirect(route('admin.arrival_countries.index'));

    }
}
