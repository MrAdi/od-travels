<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Http\Controllers\Controller;
use App\Requirments;
use App\Visa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisaRequirmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $countrys=Country::all();
        $visas = Visa::all();
        return view('admin.pages.visa_requirements.index',compact('visas','countrys'));
    }

    function fetch(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $visas = DB::table('visas')
            ->where($select, $value)
            ->get();
        $output = '<option value="">Select '.ucfirst($dependent).'</option>';
        foreach($visas as $row)
        {
            $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
        }
        echo $output;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys=Country::all();
        $visas=Visa::all();
        $requirments=Requirments::all();
        return view('admin.pages.visa_requirements.create',compact('visas','requirments','countrys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'visa_id'=>'required',
            'requirment_id'=>'required',

        ]);

        $visa = Visa::findOrFail($request->visa_id);
        if ($visa){
            $visa->requirments()->sync($request->requirment_id);
        }
        return redirect(route('admin.visa_requirment.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requirments  $visa_requirment
     * @return \Illuminate\Http\Response
     */
    public function show(Requirments $requirments)
    {
        return view('admin.pages.visa_requirements.show',compact('requirments'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Requirments $requirments
     * @return \Illuminate\Http\Response
     * @internal param $visa_requirment
     */
    public function edit($visa_id)
    {
        $visa = Visa::find($visa_id);
        $requirments=Requirments::all();
        $visa_requirments=$visa->requirments;
        $visa_requirmentsArray= [];
        foreach ($visa_requirments as $visa_requirment){
            $visa_requirmentsArray[$visa_requirment->id] = $visa_requirment->name;
        }
        $visa_requirments = $visa_requirmentsArray;
        return view('admin.pages.visa_requirements.edit',compact('visa','requirments','visa_requirments'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Requirments $requirments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$visa_id)
    {
        $visa = Visa::findOrFail($visa_id);
        if ($visa){
            $visa->requirments()->sync($request->requirments);
        }
        return redirect(route('admin.visa_requirment.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requirments $requirments)
    {

    }
}
