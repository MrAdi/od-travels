<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Country;
use App\Visa;
use Illuminate\Http\Request;

class VisaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countrys=Country::all();
        $adis=Visa::orderBy('id','dec')->get();
        return view('admin.pages.visa.index',compact('adis','countrys'));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys=Country::all();
        return view('admin.pages.visa.create',compact('countrys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'country_id'=>'required',
            'name'=>'required',
            'banner_url'=>'required',
//           'banner_name'=>'required',
            'banner_heading'=>'required',
            'description'=>'required',
            'processing_time'=>'required',
        ]);
        if ($request->file('banner_url')){
            $file = $this->uploadFile('uploads/visa/',$request->file('banner_url'));
        }
        Visa::create([

            'country_id'=>$request->country_id,
            'name'=>$request->name,
            'banner_url'=>$file['file_url'],
            'banner_heading'=>$request->banner_heading,
            'description'=>$request->description,
            'processing_time'=>$request->processing_time,


        ]);

        return redirect(route('admin.visa.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visa  $visa
     * @return \Illuminate\Http\Response
     */
    public function show(Visa $visa)
    {
        return view('admin.pages.visa.show',compact('visa'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visa  $visa
     * @return \Illuminate\Http\Response
     */
    public function edit(Visa $visa)
    {
        $countrys=Country::all();
        return view('admin.pages.visa.edit',compact('visa','countrys'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visa  $visa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visa $visa)
    {
        if ($request->file('banner_url')){
            $this->removeFile($visa->banner_url);
            $file = $this->uploadFile('uploads/visa/',$request->file('banner_url'));
            $visa->banner_url=$file['file_url'];
        }



        $visa->country_id=$request->country_id;
        $visa->name=$request->name;
        $visa->banner_heading=$request->banner_heading;
        $visa->description=$request->description;
        $visa->processing_time=$request->processing_time;
        $visa->save();
        return redirect(route('admin.visa.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visa  $visa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visa $visa)
    {
        $visa->delete();
        return redirect(route('admin.visa.index'));
    }
}
