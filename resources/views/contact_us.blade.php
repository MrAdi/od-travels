@extends('layouts.app')

@section('content')

    <div class="home-1">

        <img src="{{asset('images/contact.jpg')}}" >
        {{--<div class="rainbow-div"><h1 class="rainbow">CONTACT US</h1></div>--}}

        {{--<h1>CONTACT US</h1>--}}

    </div>
    <div class="contact_form_section">
        <div class="container">
            <div class="row">
                <div class="col">

                    <!-- Contact Form -->
                    <div class="contact_form_container">
                        <div class="contact_title text-center">get in touch</div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <form  method="post" action="{{url('contact_us/send')}}" id="contact_form" class="contact_form text-center">
                            {{ csrf_field() }}
                            <input type="text" name="name" id="contact_form_name" class="contact_form_name input_field" name="name" placeholder="Name" required="required" data-error="Name is required.">
                            <input type="text" name="email" id="contact_form_email" class="contact_form_email input_field" name="email" placeholder="E-mail" required="required" data-error="Email is required.">
                            <input type="text" name="phone" id="contact_form_phone" class="contact_form_subject input_field" name="phone" placeholder="Phone No" required="required" data-error="Phone No is required.">
                            <input type="text" name="subject" id="contact_form_subject" class="contact_form_subject input_field" name="subject" placeholder="Subject" required="required" data-error="Subject is required.">
                            <textarea name="message" id="contact_form_message" class="text_field contact_form_message" name="message" rows="4" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
                            <button type="submit" name="send" id="form_submit_button" name="submit" class="form_submit_button button trans_200">send message<span></span><span></span><span></span></button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- About -->
    <div class="about">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>

    <!-- Google Map -->

    <div class="odtravel_map">
        <div class="map_container">
            <div id="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d212.59155669480708!2d74.29040872590409!3d31.511388774497398!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa29b6e58736928b1!2sOD+Travels!5e0!3m2!1sen!2s!4v1548939150603" width="100%" height="500" frameborder="0" style="border:1px solid gray" allowfullscreen></iframe>
            </div>
        </div>

    </div>

@endsection