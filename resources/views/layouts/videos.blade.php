<section id="latest-pkg">
    <div class="container">
        <h1>Our Videos</h1>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="arrow">
                <a class="carousel-control-prev1" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next1" href="#carouselExampleControls" role="button" data-slide="next">
                    <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            @php
                $tour_videos_count = count($tour_videos);
                $tour_videos_count_items = (integer)($tour_videos_count/4);
            @endphp
            <div class="carousel-inner">
                @for($i=1; $i<=$tour_videos_count_items ; $i++)

                <div class="carousel-item {{ $i == 1 ? 'active' : '' }}">

                    <div class="row">
                        @php
                            $lastIndex = $i * 4;
                            $firstIndex = ($i - 1) * 4;
                        @endphp
                        @for($j=0; $j<4 ; $j++)

                        {{--@foreach($tour_videos as $tour_video)--}}

                            <div class="col-md-3 thumbnail" style="width:255px ; height:189px;">
                                {!! $tour_videos[$firstIndex + $j]['url'] !!}
                                {{--{{asset( $tour_videos[$firstIndex + $j]['url']) }}--}}
                                {{--{{dd($tour_videos_count_items)}}--}}
                                {{--{!! $tour_video->url !!}--}}
                            </div>
                        {{--@endforeach--}}
                        @endfor

                    </div>

                </div>
                @endfor

            </div>
        </div>
    </div>
</section>