<marquee class="marquee">

    <img src="{{asset('images/img1.png')}}">
    <img src="{{asset('images/img2.png')}}">
    <img src="{{asset('images/img3.png')}}">
    <img src="{{asset('images/img4.png')}}">
    <img src="{{asset('images/img5.png')}}">
    <img src="{{asset('images/img6.png')}}">
    <img src="{{asset('images/img7.png')}}">
    <img src="{{asset('images/img8.png')}}">
    <img src="{{asset('images/img9.png')}}">
    <img src="{{asset('images/img10.png')}}">
    <img src="{{asset('images/img11.png')}}">
    <img src="{{asset('images/img12.png')}}">
    <img src="{{asset('images/img13.png')}}">
    <img src="{{asset('images/img14.png')}}">

</marquee>

<footer class="footer">
    <img src="{{asset('images/foot.jpg')}}">
    <div class="back-foot">
        <div class="container">
            <div class="row footer_row">

                <!-- Footer Column -->
                <div class="col-lg-6 footer_column">
                    <div class="footer_col">
                        <div class="footer_content footer_about">
                            <div class="logo_container footer_logo">
                                <div class="logo"><img src="{{asset('images/logo.png')}}" alt=""></div>
                            </div>
                            <p class="footer_about_text">
                                {{$contacts->text}}
                            </p>
                            <ul class="footer_social_list">

                                <li class="footer_social_item"><a target="_blank" href="{{$contacts->facebook}}"><i class="fa fa-facebook-f"></i></a></li>
                                <li class="footer_social_item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="footer_social_item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <!-- Footer Column -->
                <div class="col-lg-6 footer_column text-center">
                    <div class="footer_col">
                        <div class="footer_title">contact info</div>
                        <div class="footer_content footer_contact">
                            <ul class="contact_info_list">
                                <li class="contact_info_item d-flex flex-row">
                                    <div><div class="contact_info_icon"><img src="{{asset('images/placeholder.svg')}}" alt=""></div></div>
                                    <div class="contact_info_text">
                                        {{$contacts->address}}
                                    </div>
                                </li>
                                <li class="contact_info_item d-flex flex-row">
                                    <div><div class="contact_info_icon"><img src="{{asset('images/phone-call.svg')}}" alt=""></div></div>
                                    <div class="contact_info_text">
                                        {{$contacts->mobile}}
                                    </div>
                                </li>
                                <li class="contact_info_item d-flex flex-row">
                                    <div><div class="contact_info_icon"><img src="{{asset('images/message.svg')}}" alt=""></div></div>
                                    <div class="contact_info_text">
                                        {{$contacts->email}}
                                    </div>
                                </li>
                                <!--<li class="contact_info_item d-flex flex-row">-->
                                <!--<div><div class="contact_info_icon"><img src="images/planet-earth.svg" alt=""></div></div>-->
                                <!--<div class="contact_info_text"><a href="#">www.odTravel.com</a></div>-->
                                <!--</li>-->
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Copyright -->
    {{--<div class="footer-copyright text-center py-3">© 2020 Copyright:--}}
        {{--<a href="#"> Odbytes.com</a>--}}
    {{--</div>--}}

    <!-- Copyright -->
</footer>
<div class="footer-copyright text-center text-black-50 py-3" style="background: linear-gradient(to right, #2b720f, #f4511e);">© 2019 OD Travels. All Rights Reserved.<br>Powered by
     <a class="dark-grey-text" href="#">Odbytes</a>
</div>