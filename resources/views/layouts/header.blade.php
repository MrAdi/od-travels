<div class="super_container">

    <!-- Header -->
    <header class="header" id="top-fix">

        <!-- Top Bar -->

        <div class="top_bar">
            <div class="container">
                <div class="row">
                    <div class="col d-flex flex-row">
                        <div class="phone"><span> <i class="fa fa-phone" aria-hidden="true"></i> call us:</span>
                            {{$contacts->mobile}}
                        </div>
                        <div class="social">
                            <ul class="social_list">
                                <li class="social_list_item"><a target="_blank" href="{{$contacts->facebook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="social_list_item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="social_list_item"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="user_box ml-auto">
                            <span> <i class="fa fa-envelope" aria-hidden="true"></i>Email:</span>
                            {{$contacts->email}}
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo2.png') }}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span>
    	<i class="fa fa-bars" aria-hidden="true"></i>
    </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-item nav-link" href="{{ url('about_us') }}">About</a>
                        </li>
                        <li class=" nav-item dropdown">
                            <a href="#" class=" nav-link" data-toggle="dropdown">Visa
                                <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <div class="dropdown-menu mega-menu">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td><img src="{{ asset('images/chinaflag.jpg') }}"> <span><a href="{{ url('visa/1') }}"><b>China</b></a></span> </td>
                                        <td><img src="{{ asset('images/malaysia-Flag.jpg') }}"> <span><a href="{{ url('visa/3') }}"><b>Malaysia</b></a></span> </td>
                                        <td><img src="{{ asset('images/singapore-flag.jpg') }}"> <span><a href="{{ url('visa/5') }}"><b>Singapore</b></a></span> </td>

                                    </tr>
                                    <tr>
                                        <td><img src="{{ asset('images/indonesiaflag.jpg') }}"> <span><a href="{{ url('visa/6') }}"><b>Indonasia</b></a></span> </td>
                                        <td><img src="{{ asset('images/egypt-flag.png') }}"> <span><a href="{{ url('visa/8') }}"><b>Egypt</b></a></span> </td>
                                        <td><img src="{{ asset('images/vietnamflag.jpg') }}"> <span><a href="{{ url('visa/20') }}"><b>Vietnam</b></a></span> </td>

                                    </tr>
                                    <tr>
                                        <td><img src="{{ asset('images/uzbekistan-flag.jpg') }}"> <span><a href="{{ url('visa/21') }}"><b>Uzbekistan</b></a></span> </td>
                                        <td><img src="{{ asset('images/mauritius-flag.jpg') }}"> <span><a href="{{ url('visa/14') }}"><b>Mauritius</b></a></span> </td>
                                        <td><img src="{{ asset('images/turkishflag.jpg') }}"> <span><a href="{{ url('visa/10') }}"><b>Turky</b></a></span> </td>
                                    </tr>
                                    <tr>
                                        <td><img src="{{ asset('images/thailandflag.jpg') }}"> <span><a href="{{ url('visa/4') }}"><b>Thailand</b></a></span> </td>
                                        <td><img src="{{ asset('images/dubai-flag.jpg') }}"> <span><a href="{{ url('visa/9') }}"><b>Dubai</b></a></span> </td>
                                        <td><img src="{{ asset('images/Philippineflag.jpg') }}"> <span><a href="{{ url('visa/16') }}"><b>Philippines</b></a></span> </td>
                                    </tr>

                                    </tbody>
                                </table>
                                <a class="dropdown-item" style="text-align: center" href="{{ url('visa_category') }}"><b>More</b></a>
                            </div>
                        </li><!-- /.dropdown -->

                        <li class="nav-item">
                            <a class="nav-item nav-link" href="{{ url('tour_packages') }}">Tours</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-item nav-link" href="{{ url('umraah') }}">Umrah</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-item nav-link" href="{{ url('arrival_countries') }}">On Arrival Countries</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-item nav-link" href="{{ url('insaurance') }}">Insaurance</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-item nav-link" href="{{ url('domastic_tour_packages') }}">Domestic Tours</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-item nav-link" href="{{ url('contact_us') }}">Contact</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </header>
</div>