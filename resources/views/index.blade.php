@extends('layouts.app')

@section('content')


<div class="wrap">
	<div class="js-conveyor-example">
		<ul>
			<li>
				<span>Air Tickets, Travel Insurance | Hotel Booking, Tour Packages | Visa Information, Umrah Packages</span>
			</li>
			<li>
				<a href="#">
					<span>OD travels</span>
				</a>
			</li>
			<li>
				<span>Call Us :{{$contacts->mobile}}</span>
			</li>

		</ul>
	</div>
</div>



<div class="home">

	<!-- Home Slider -->

	<div class="home_slider_container">

		<div id="myowl" class="owl-carousel owl-theme home_slider">
			<!-- Slider Item -->
			@foreach($home_sliders as $home_slider)
			<div class="owl-item home_slider_item">
				<div class="home_slider_background"><img src="{{$home_slider->avatar}}"></div>
			</div>
			@endforeach
		</div>

	</div>

	<!-- Home Slider Nav - Prev -->
	<div class="home_slider_nav home_slider_prev">
		<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
					<defs>
						<linearGradient id='home_grad_prev'>
							<stop offset='0%' stop-color='#2b720f'/>
							<stop offset='100%' stop-color='#f4511e'/>
						</linearGradient>
					</defs>
			<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
					M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
					C22.545,2,26,5.541,26,9.909V23.091z"/>
			<polygon class="nav_arrow" fill="#F3F6F9" points="15.044,22.222 16.377,20.888 12.374,16.885 16.377,12.882 15.044,11.55 9.708,16.885 11.04,18.219
					11.042,18.219 "/>
				</svg>
	</div>

	<!-- Home Slider Nav - Next -->
	<div class="home_slider_nav home_slider_next">
		<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
					<defs>
						<linearGradient id='home_grad_next'>
							<stop offset='0%' stop-color='#2b720f'/>
							<stop offset='100%' stop-color='#f4511e'/>
						</linearGradient>
					</defs>
			<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
				M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
				C22.545,2,26,5.541,26,9.909V23.091z"/>
			<polygon class="nav_arrow" fill="#F3F6F9" points="13.044,11.551 11.71,12.885 15.714,16.888 11.71,20.891 13.044,22.224 18.379,16.888 17.048,15.554
				17.046,15.554 "/>
				</svg>
	</div>

</div>


<div class="search">


	<!-- Search Contents -->

	<div class="container fill_height">
		<div class="row fill_height">
			<div class="col fill_height">




				<!-- Search Panel -->

				<div class="search_panel active">
					<form action="{{ route('search.tour') }}" method="post" id="search_form_1" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center  align-items-lg-center justify-content-lg-center justify-content-center">
						{{--{{csrf_field()}}--}}
						<div class="search_item">
							<div> Enter Destination</div>
							<select name="heading" id="adults_2" class="dropdown_item_select search_input" >
								@foreach($tours as $tour)
									<option value="{{$tour->heading}}">{{$tour->heading}}</option>
								@endforeach
							</select>
						</div>
						<div class="search_item">
							<div> Enter price</div>
							<input type="text" name="price" class="check_in search_input" placeholder="price" >
						</div>

						<button class="button search_button" >search</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@if(isset($tours_search))
	<div class="container">
		<h2>Search Result</h2>

		<div class="table-responsive">

			<table class="table table-responsive table-hover">
				<thead>
				<tr>
					<th>Name</th>
					<th>Price</th>
					<th>Description</th>
					<th>Minimum Allowed</th>

				</tr>
				</thead>
				<tbody>
				@foreach($tours_search as $tour)
					<tr>
						<td>{{$tour->heading}}</td>
						<td>{{$tour->price}}</td>
						<td>{{$tour->description}}</td>
						<td>{{$tour->min_allowed}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endif


<section id="news">
	<div class="container">
		<p>LATEST NEWS</p>
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
			@php
				$news_count = count($news);
                $news_count_items = (integer)($news_count/4);
			@endphp

			<div class="carousel-inner">
				@for($i=1; $i<=$news_count_items ; $i++)

				<div class="carousel-item {{ $i == 1 ? 'active' : '' }}">

					<div class="row">
						@php
							$lastIndex = $i * 4;
                            $firstIndex = ($i - 1) * 4;
						@endphp
						@for($j=0; $j<4 ; $j++)

						<div class="col-md-3">

							<div class="img-container">
								<img src="{{asset($news[$firstIndex + $j]['image'])}}">
							</div>


							<span class="rating">
										<a href="#">{{ $news[$firstIndex + $j]['title']}}</a>
							</span>
							<h4 class="media-heading">
								<a href="#">{{ $news[$firstIndex + $j]['text']}}</a>
							</h4>
						</div>
						@endfor
					</div>
				</div>
				@endfor
			</div>
			<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</section>


{{--<section id="news">--}}
	{{--<p>LATEST NEWS</p>--}}
	{{--<div class="container">--}}
		{{--<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">--}}
			{{--@php--}}
			{{--$news_count = count($news);--}}
			{{--$news_count_items = (integer)($news_count/4);--}}
			{{--@endphp--}}
			{{--<div class="carousel-inner">--}}
				{{--@for($i=1; $i<=$news_count_items ; $i++)--}}

				{{--<div class="carousel-item {{ $i == 1 ? 'active' : '' }}">--}}
					{{--<div class="row">--}}
						{{--@php--}}
							{{--$lastIndex = $i * 4;--}}
                            {{--$firstIndex = ($i - 1) * 4;--}}
						{{--@endphp--}}
						{{--@for($j=0; $j<4 ; $j++)--}}
						{{--@foreach($news as $new)--}}
						{{--<div class="col-md-3">--}}
							 {{--<div class="row">--}}
								{{--<div class="col-md-6">--}}
									{{--<img src="{{asset($news[$firstIndex + $j]['image'])}}" style="width: 113px ; height: 113px">--}}
								{{--</div>--}}
								{{--<div class="col-md-6">--}}
									{{--<h4 class="media-heading">--}}
										{{--<a href="#">{{ $news[$firstIndex + $j]['title']}}</a>--}}
									{{--</h4>--}}
									{{--<h3 class="media-heading">--}}
										{{--<a href="#">{{ $news[$firstIndex + $j]['text']}}</a>--}}
									{{--</h3>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
						{{--@endforeach--}}
						{{--@endfor--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--@endfor--}}
			{{--</div>--}}
			{{--<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">--}}
				{{--<span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
				{{--<span class="sr-only">Previous</span>--}}
			{{--</a>--}}
			{{--<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">--}}
				{{--<span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
				{{--<span class="sr-only">Next</span>--}}
			{{--</a>--}}
		{{--</div>--}}
	{{--</div>--}}
{{--</section>--}}

<section id="tour">
	<div class="container">
		<h1>WE Have The Best Tours</h1>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam <br>nonummy nibh euismod tincidunt ut laoreet </p>
		<div class="row">
			@foreach($tours as $tour)
			<div class="col-md-3 col-sm-4 pkg" style="margin-top: 28px;">
				<img src="{{ $tour->cover_url}}" style="width: 256px ; height: 188px">
				<h2>{{ $tour->heading}}</h2>
			</div>
			@endforeach

		</div>
	</div>
</section>
<div class="cta">
<!-- Image by https://unsplash.com/@thanni -->
	<div class="cta_background"> <img src=""> </div>

	<div class="container">
		<div class="row">
			<div class="col">

				<!-- CTA Slider -->

				<div class="cta_slider_container">
					<div class="owl-carousel owl-theme cta_slider">

					@foreach($tours as $tour)
						<!-- CTA Slider Item -->
						<div class="owl-item cta_item text-center">
							<img src="{{ $tour->cover_url}}">
							<div class="cta_title">{{$tour->heading}}</div>
							<p class="cta_text">{{$tour->description}}</p>
							<div class="button cta_button"><div class="button_bcg"></div><a href="{{ url('contact_us') }}">book now</a></div>
						</div>
					@endforeach

					</div>

					<!-- CTA Slider Nav - Prev -->
					<div class="cta_slider_nav cta_slider_prev">
						<svg version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
								<defs>
									<linearGradient id='cta_grad_prev'>
										<stop offset='0%' stop-color=' #2E7D32'/>
										<stop offset='100%' stop-color='#F57C00'/>
									</linearGradient>
								</defs>
							<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
								M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
								C22.545,2,26,5.541,26,9.909V23.091z"/>
							<polygon class="nav_arrow" fill="#F3F6F9" points="15.044,22.222 16.377,20.888 12.374,16.885 16.377,12.882 15.044,11.55 9.708,16.885 11.04,18.219
								11.042,18.219 "/>
							</svg>
					</div>

					<!-- CTA Slider Nav - Next -->
					<div class="cta_slider_nav cta_slider_next">
						<svg version="1.1" id="Layer_5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
								<defs>
									<linearGradient id='cta_grad_next'>
										<stop offset='0%' stop-color=' #2E7D32'/>
										<stop offset='100%' stop-color='#F57C00'/>
									</linearGradient>
								</defs>
							<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
							M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
							C22.545,2,26,5.541,26,9.909V23.091z"/>
							<polygon class="nav_arrow" fill="#F3F6F9" points="13.044,11.551 11.71,12.885 15.714,16.888 11.71,20.891 13.044,22.224 18.379,16.888 17.048,15.554
							17.046,15.554 "/>
							</svg>
					</div>

				</div>

			</div>
		</div>
	</div>

</div>
<div class="offers">
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<h2 class="section_title">the best offers</h2>
			</div>
		</div>
		<div class="row offers_items">

			<!-- Offers Item -->
			@foreach($offers as $offer)
			<div class="col-lg-6 offers_col">
				<div class="offers_item">
					<div class="row">
						<div class="col-lg-6">
							<div class="offers_image_container">
							<!-- Image by https://unsplash.com/@kensuarez -->
								<div class="offers_image_background" style="background-image:url({{$offer->avatar}}); width: 285px; Height: 220px;"></div>
								<div class="offer_name" style="top: 165px"><a href="#">{{$offer->heading}}</a></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="offers_content">
								<div class="offers_price" style="margin-top: 20px;">{{$offer->price}}</div>

								<p class="offers_text">{{$offer->text}}.</p>

								{{--<div class="offers_link"><a href="offers.html">read more</a></div>--}}
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
<div class="testimonials">
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<h2 class="section_title">what our clients says about us</h2>
			</div>
		</div>
		<div class="row">
			<div class="col">

				<!-- Testimonials Slider -->

				<div class="test_slider_container">
					<div class="owl-carousel owl-theme test_slider">

						<!-- Testimonial Item -->
						@foreach($testimonials as $testimonial)

						<div class="owl-item">
							<div class="test_item">
								<div class="test_image"><img src="{{ asset($testimonial->avatar) }}" style="height: 484px; width: 350px;" alt="https://unsplash.com/@anniegray"></div>
								<div class="test_content_container">
									<div class="test_content">
										<div class="test_item_info">
											<div class="test_name">{{$testimonial->name}}</div>
											<div class="test_date">{{$testimonial->date}}</div>
										</div>
										<div class="test_quote_title">"{{$testimonial->title}}"</div>
										<p class="test_quote_text">{{$testimonial->text}}</p>
									</div>
								</div>
							</div>
						</div>
					@endforeach


					</div>

					<!-- Testimonials Slider Nav - Prev -->
					<div class="test_slider_nav test_slider_prev">
						<svg version="1.1" id="Layer_6" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
								<defs>
									<linearGradient id='test_grad_prev'>
										<stop offset='0%' stop-color=' #2E7D32'/>
										<stop offset='100%' stop-color='#F57C00'/>
									</linearGradient>
								</defs>
							<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
								M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
								C22.545,2,26,5.541,26,9.909V23.091z"/>
							<polygon class="nav_arrow" fill="#F3F6F9" points="15.044,22.222 16.377,20.888 12.374,16.885 16.377,12.882 15.044,11.55 9.708,16.885 11.04,18.219
								11.042,18.219 "/>
							</svg>
					</div>

					<!-- Testimonials Slider Nav - Next -->
					<div class="test_slider_nav test_slider_next">
						<svg version="1.1" id="Layer_7" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
								<defs>
									<linearGradient id='test_grad_next'>
										<stop offset='0%' stop-color=' #2E7D32'/>
										<stop offset='100%' stop-color='#F57C00'/>
									</linearGradient>
								</defs>
							<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
							M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
							C22.545,2,26,5.541,26,9.909V23.091z"/>
							<polygon class="nav_arrow" fill="#F3F6F9" points="13.044,11.551 11.71,12.885 15.714,16.888 11.71,20.891 13.044,22.224 18.379,16.888 17.048,15.554
							17.046,15.554 "/>
							</svg>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>


@endsection

