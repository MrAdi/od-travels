@extends('layouts.app')

@section('content')

<div class="home-1">

    <img src="{{asset('images/tour.jpg')}}">


    {{--<img src="{{asset($tour_detail->cover_url)}}">--}}
    {{--<div class="rainbow-div"><h1 class="rainbow">Tour Detail</h1></div>--}}



</div>
<section id="pkg-detail">
    <div class="container">

        <!-- main slider carousel -->
        <div class="row">
            <div class="col-lg-6" id="slider">
                {{--<div id="myCarousel" class="carousel slide">--}}
                    {{--<!-- main slider carousel items -->--}}
                    {{--@foreach($tour_image as $image)--}}
                        {{--{{dd($image)}}--}}
                    {{--<div class="carousel-inner">--}}
                        {{--<div class="active item carousel-item" data-slide-number="0">--}}
                            {{--<img src="{{asset($image->url)}}" class="img-fluid">--}}
                        {{--</div>--}}


                        {{--<a class="carousel-control-prev" href="#myCarousel" data-slide="prev">--}}
                            {{--<span class="carousel-control-prev-icon"></span>--}}
                        {{--</a>--}}
                        {{--<a class="carousel-control-next" href="#myCarousel" data-slide="next">--}}
                            {{--<span class="carousel-control-next-icon"></span>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                    {{--<!-- main slider carousel nav controls -->--}}
                {{--</div>--}}

                <div id="myCarousel" class="carousel slide">
                    <!-- main slider carousel items -->
                    <div class="carousel-inner">

                        @foreach($tour_image as $index => $image)
                            <div class="{{ $index == 1 ? 'active' : '' }} item carousel-item" data-slide-number="{{ $index }}">
                                <img src="{{asset($image->url)}}" class="img-fluid" style="height: 540px; width: 540px">
                            </div>
                        @endforeach

                        <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>

                    </div>
                    <!-- main slider carousel nav controls -->


                    <ul class="carousel-indicators list-inline">
                        @foreach($tour_image as $index => $image)
                            <li class="list-inline-item {{ $index == 1 ? 'active' : '' }}">
                                <a id="carousel-selector-{{ $index }}" class="selected" data-slide-to="{{ $index }}" data-target="#myCarousel">
                                    <img src="{{asset($image->url)}}" class="img-fluid" style="height: 60px; width: 60px">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="book-now">
                    <h1>Book Now</h1>
                    <p>Call us at <span>{{$contacts->mobile}}</span>, or fill out the form below and we'll get back to you as soon as possible.</p>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    <form id="con-form" method="post" action="{{url('contact_us/send')}}">
                        {{ csrf_field() }}

                        <div class="form-row">
                            <div class="form-group  col-md-6">

                                <input type="text" name="name" class="form-control form-control-lg" id="name" placeholder=" First Name*"  minlength="3" required >
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="Email*" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" name="phone" class="form-control form-control-lg" id="phone" placeholder="phone*" minlength="7"  maxlength="12" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="subject" class="form-control form-control-lg" id="Subject" placeholder="Subject" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea name="message" aria-required="true" aria-invalid="true" placeholder="Comments*"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <center>
                                    <input type="submit" name="send" class="btn btn-primary btn-lg" value="Book Now">
                                </center>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
            <!-- end of column -->

            <div class="project-content col-md-6">
                <h4><span>Package Price Starting From:</span></h4>
                <p style="font-size:30px; color: #009444; font-weight:bold;"><span class="pkr">PKR</span>{{$tour_detail->price}}</p>
                <h4><span>Package Details:</span></h4>
                <ul>
                    <li><strong>Package Title: </strong>{{$tour_detail->package_title}}</li>
                    <li><strong>Departure Countries: </strong>{{$tour_detail->departure_country}}</li>
                    <li><strong>Departure Cities: </strong>{{$tour_detail->departure_city}}</li>
                    <li>
                        <strong>Min Allowed: </strong>
                        {{$tour_detail->min_allowed}}      </li>
                    <li>
                        <strong>Ticket: </strong>
                        {{$tour_detail->ticket}}      </li>
                    <li>
                        <strong>Visa: </strong>
                        {{$tour_detail->visa}}      </li>
                    <li>
                        <strong>Insurance: </strong>
                        {{$tour_detail->insurance}}      </li>
                    <li><strong>Stay: </strong>{{$tour_detail->stay}}</li>
                    <li><strong>Hotel Choice: </strong>{{$tour_detail->hotel}}</li>
                    <li><strong>Company: </strong> {{$tour_detail->company}}</li>
                    <li><strong>Rate mentioned: </strong>
                        {{$tour_detail->rate_mentioned}}
                    </li>
                    <li><strong>Valid Till: </strong>{{$tour_detail->valid_till}}</li>
                    <li><strong>Posted on: </strong>{{$tour_detail->posted_on}}</li>
                </ul>

                <div class="panel-group" id="accordion" style="margin-bottom:0px;">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1" class="" aria-expanded="true">
                                        <i class="fa fa-folder-open-o"></i>
                                        Package Description:
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-1" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <ul>
                                        <li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{$tour_detail->description}}</strong></li>
                                        <br>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="" aria-expanded="true">
                                        <i class="fa fa-folder-open-o"></i>
                                        Package Included:
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-2" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <ul>

                                        <li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{$tour_detail->pkg_includes}} </strong></li>
                                        <br>
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> 2 Nights at Melbourne.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> 2 Nights at Sydney.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> 2 Nights at Brisbane.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> All Ciites Transfers Airport to Hotel and Airport.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> City, Harbour and Bondi Beach Tour.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sydney Opera House.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Puffing Bily Morning .</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Koala and River Cruise.</strong></li>--}}
                                        {{--<br>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4" class="" aria-expanded="true">
                                        <i class="fa fa-fax"></i>
                                        Package Exclusions:
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-4" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <ul>

                                        <li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> {{$tour_detail->pkg_exclusive}}</strong></li>
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Beverages NOT included in the quotation.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Entrance Fee (unless specified).</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Driver &amp; Guide Tipping.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Room service.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Early Check-in.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Late Check-out.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Luggage truck &amp; Porter.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> 50% supplement for arrival/departure transfer between 2200 hrs - 0700 hrs.</strong></li>--}}
                                        {{--<br>--}}
                                        {{--<li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> If City / Resort Fees / Tourist taxes are applicable then the same will be payable directly at hotel /apartment.</strong></li>--}}
                                        {{--<br>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-6" class="" aria-expanded="true">
                                        <i class="fa fa-bookmark"></i>
                                        Contact Info:
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-6" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="">
                                        <ul class="icons-list">
                                            <li><i class="fa fa-phone"></i> <strong>UAN: </strong><a href="tel:{{$contacts->mobile}}">{{$contacts->mobile}}</a></li>
                                            <li><i class="fa fa-headphones"></i> <strong>Sales and Support: </strong><a href="tel:{{$contacts->mobile}}">{{$contacts->mobile}}</a> </li>
                                            <li><i class="fa fa-whatsapp"></i> <strong>Whatsapp: </strong><a href="tel:{{$contacts->mobile}}">{{$contacts->mobile}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="" aria-expanded="true">
                                        <i class="fa fa-fax"></i>
                                        Terms and Conditions:
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-5" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <ul>
                                        <li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hotel standard Check-In / Check-out time 1400 hrs and 1200 hrs respectively.</strong></li>
                                        <br>
                                        <li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Prices are subject to change with any sudden increase in the cost by hotel &amp; transportation department and any new tax by the government. Rates include only those items specified in your itinerary.</strong></li>
                                        <br>
                                        <li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Above rates are valid on a minimum of 2 adults travelling together.</strong></li>
                                        <br>
                                        <li><strong><i class="fa fa-angle-double-right" aria-hidden="true"></i> Strictly No refund for un-utilized services.</strong></li>
                                        <br>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>





            </div>
        </div>
        <!--/main slider carousel-->
    </div>
</section>
@include('layouts.videos')

@endsection