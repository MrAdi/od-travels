@extends('layouts.app')

@section('content')

    <div class="home-1">

        <img src="{{asset('images/about.jpg')}}">
        {{--<div class="rainbow-div"><h1 class="rainbow">ABOUT US</h1></div>--}}

        {{--<h1>ABOUT US</h1>--}}

    </div>
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="intro_image"><img src="{{asset('images/intro.png')}}" alt="" style="margin-top: 100px;"></div>
                </div>
                <div class="col-lg-5">
                    <div class="intro_content">
                        <div class="intro_title">we have the best tours</div>
                        <p class="intro_text">
                            OdTravel is an online travel and booking agency which aims to provide best solutions for the empowerment of online travel world. We assist people in getting work visas for other countries and take them to the most stunning Asian tourist destinations where they can spend holidays in tranquility and peace. If you are planning a business tour or a holiday trip, we will not only arrange best of transportation modes but will also save you money on hotel deals and fares.
                        </p>

                        <ul>
                            <h2>Here are some bright reasons why people love to acquire our service for foreign trips:</h2>
                            <li>Your best interest is our topmost priority. We have a rich history of fighting for our clients and our agents are obliged to follow a strict code of ethics.</li>

                            <li>Strong ties with other travel agents and agencies to access the latest information in order to give you the best value for money.</li>

                            <li> We have travel agents throughout the country to assist with your traveling needs.</li>

                            <li> Our travel agents are knowledgeable, educated, and active in the industry. We train them well and equip them with tools to serve you in the most professional manner.</li>

                            <li> We will do all we can to serve your travel needs and go an extra mile to help our clients.
                            </li>
                        </ul>
                        <h2>
                            In short, our priority is your best interest. We have a history of providing top-notch services. Should you ever need assistance regarding traveling, OdTravel is here to help you.</h2>




                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection