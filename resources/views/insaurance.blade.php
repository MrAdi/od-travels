@extends('layouts.app')

@section('content')

<div class="super_container">
<div class="home-1">

    <img src="{{asset('images/insur.jpg')}}">
    {{--<div class="rainbow-div"><h1 class="rainbow">INSURANCE</h1></div>--}}

    {{--<h1>INSURANCE</h1>--}}

</div>
<section id="insaurance">

    <div class="container">
        <div class="travel-insurance">
        <img src="{{asset($insaurance->picture)}}">
        </div>
        <div class="travel-insurance">
            <h1>Travel insurance</h1>
            <p>Whenever and wherever you travel for any purpose whether for personal reasons or business objectives, there involves a risk. No matter you are travelling in some other country, on some other continent, province or even to some other city, the risk is always there.</p>
            <p>Travel insurance comes to cover the risk and the losses that can be caused due to your travel and tour to some destination. It also covers the financial losses which occurs even before or after your tour, whether it was a personal tour or some business trip. Typical travel insurance covers medical expenses during travel, damage or loss of your baggage, loss in case of theft, extra expenditures in case of cancellation, etc.</p>
            <p>You should never take the travel insurance casually; rather it is one of the musts whenever you plan to travel towards any destination. No matter you are a regular traveller or travelling for the first and last time, you must have some good travel insurance policy for your safety.</p>
            <p>Most of the governments in the world suggest you buy travel insurance whenever you travel abroad for example travel insurance for USA. You shouldn’t forget the fact that best travel insurance is must to have a thing like your passport if you are travelling abroad. Not just abroad, you must opt to have good travel insurance even if you are travelling within the country.</p>
            <p>It’s always a good idea to buy the travel insurance at the time of payment for the trip. It gives you extra coverage in case of cancellation of your trip due to any serious reasons including illness. If you had bought travel insurance at the time of payment for your trip, you can claim and recover all the expenses like ticket price, accommodation expenses and other expenses that you already paid in case you fail to travel or cancel your trip for any natural reason.</p>
            <p>Approved travel insurance companies offer different types of travel insurance policies which offer different types of coverage.  You can get the policy which may cover all your family members including spouse and children. If you are a regular traveller, you can buy the travel insurance which can give you coverage for multiple trips for a certain period like six months or a year. For a pregnant woman, there can be some special policy for extra coverage. You can find out more about all these policies on the web and compare different travel visa insurance policies.</p>

            <h1>Domestic Travel Insurance</h1>
            <p>Most of the people don’t bother to buy travel insurance when they are travelling within the country. They feel safe with an idea in mind that they can always call for help in case of need. However, they always forget that things can go wrong anytime, and help may not reach in time.</p>
            <p>Domestic travel insurance should be given the same importance as you give to international travel insurance. Domestic travel insurance can keep you safe from the financial losses which can arise due to cancellation and delays of flights. It also covers the loss of your luggage due to mishandling or theft, before, during or even after your travel.</p>
            <p>Domestic travel insurance also covers the unexpected expense like accommodation expenses if you stuck somewhere due to some natural disaster, car rental expenses in case of emergency or even the damages to the rented car if it wasn’t due to your negligence. However, domestic travel insurance may not cover the damages and loss which occur due to adventure and sports. Rather if your trip or tour includes any adventure sports activity, you will have to pay extra travel insurance premium because of higher risk.</p>
            <h1>International Travel Insurance</h1>
            <p>Issues and problems are easy to handle if you are in your country even if you are not in your hometown but the situation changes drastically when you are not in your home country. If you travel abroad, a minor issue or mishap becomes hard to manage, mostly due to an unexpectedly high cost. In such a case, International travel insurance gives you some relief.</p>
            <p>International travel insurance gives you coverage from medical expenses when you are away from your country. It also covers the loss due to damage or theft of the luggage, loss caused by the flight delays and cancellations, accidental and death coverage as well. Travel Insurance companies in Pakistan do offer you the translator services as well for an additional cost or premium and this service can be handy when things are not going in your favour while you are in some other country with a different language.</p>
            <p>Medical treatment cost in most of the advanced countries like in the United States of America and the United Kingdom is extremely high. If you stay a single night in the hospital there, you will be charged thousands of dollars, and it can seriously damage your travel budget. If you are a foreigner, you will not get any treatment in any hospital unless you pay in advance or produce them some payment guarantee. It is some serious issue that you can’t neglect because anyone can get ill anytime and accidents and injuries are quite common when you travel abroad.</p>
            <p>South Asian countries are considered the cheapest in terms of medical treatment cost but a single day stay in the hospital in a good South Asian hospital can cost you at least $800. Moreover, yes, that’s too higher than your expectation and budget. Same medical treatment can cost your pocket about $10,000 if you are Europe or in the United Kingdom. The cost of medical treatment in the United States of America is exceptionally higher than the rest of the world. So, if you are travelling to UK or USA, it is a good idea to get travel health insurance USA or travel insurance UK to keep you covered from such unexpected medical expenses.</p>
            <p>So, what if you have to get even a minor medical treatment while you are in these countries during your trip? If hadn’t purchased any International travel insurance, you will be personally liable to bear all these medical expenses, and this can seriously affect your budget and trip plan as well. According to data provided by Insurance companies UK, just in the United Kingdom, more than 20,000 travellers every year get free medical treatment when they travel abroad.</p>
            <p>Be like these 20,000 smart travellers and don’t forget to purchase the Best Travel Insurance whenever you travel or plan to travel abroad, whether for any personal reason, vacations or even for business objectives. You can get more information about travel insurance on various websites as well where you can compare different travel insurance policies and annexed coverage and benefits as well.</p>
            <h1>Credit Card Travel Insurance</h1>
            <p>Most the credit card providing companies offer you free of charge travel insurance to its customers as a part of their credit card service but with very limited coverage. So if you want to rely on such travel insurance, read their documents carefully and check what they are offering.</p>
            <p>Not just in case of credit card travel insurance, it is always a good idea to read the policy document carefully to know about the cover you are getting after having the travel insurance policy. This document will not only reveal the amount of coverage but the conditions and requirements to qualify for this coverage as well. If you are travelling, after reading the document be assured that the coverage offered under the policy is enough for you while you will be away from your home country.</p>
            <h1>Choosing a Policy</h1>
            <p>Insurance market, especially the travel insurance market can be seen as an extremely competitive market where thousands of companies are offering different types of travel insurance policies to their customers. When a traveller plans to buy travel insurance, whether domestic or international, he has thousands of options to choose from. He needs to be vigilant in such scenario so that he may choose the travel insurance policy which may best suit his needs keeping in view his travel plan.</p>
            <p>No insurance company ever offer an unlimited coverage, so it’s good to read the policy document to know about the exact amount and qualification criteria for the coverage. It’s better to read every single clause even if you don’t have time to read. See if the policy is giving you the cover for the risk you value most. If it covers, what is the maximum amount of coverage and whether this maximum amount is enough for you or not?</p>
            <p>When you are in a competitive market, and you have thousands of options, you can easily be getting confused. Here’s the list of some questions which can help you in choosing a travel insurance policy for you:</p>
            <ul>
                <li>What risks and expenses the policy covers?</li>
                <li>What are the risks and expenses which are not covered by the policy?</li>
                <li>What is the way to contact your insurance company in case of need?</li>
                <li>What type of documents and proof do you need to produce if you need to claim?</li>
                <li>What is the maximum cover limit offered by the travel insurance company?</li>
                <li>Which of the insurance documents and other important papers do you need to carry with you?</li>
                <li>What is the actual premium amount and how much extra you are paying and why?</li>
            </ul>
            <p>Once you have found all the answers to the above questions, you will have a short list of travel insurance policies which will be perfectly matching your need. Now choose any policy from a good insurance company which is giving you complete coverage during your trip. If you feel like your stay is going to be longer than expected, its good idea to discuss the matter with your insurance company and check if they offer you an extended coverage.</p>
            <h1>Limits and Exclusions</h1>
            <p>Every travel insurance policy mentions its limits and exclusions in their policy documents. Read carefully the policy document especially the paragraph covering the limits and exclusions. Here you will find out what is not covered by the policy and what can be the circumstances where policy will not give you any coverage. Do not buy the policy if there’s any complication or ambiguity in the limits and exclusions list because it’s the section where most of the travel insurance companies play the trick.</p>
            <h1>Common Limits and Exclusions Include:</h1>
            <ul>
                <li><h2> Pre-existing Medical Conditions</h2>
                    <p>Pre-existing medical conditions are often not covered by any insurance company. A person while making a contract with the insurance company has to tell the company about these pre-existing medical conditions and illness. If a person fails to reveal such things, the insurance company may refuse to pay the medical expenses in such condition.</p>
                    <p>Some insurance companies may offer you the coverage for the pre-existing medical conditions, but such coverage will be limited as per their policy. If you had revealed the pre-existing medical condition, you might get the travel insurance, and it can give you coverage for medical expense, but it will not accept any claim for that particular illness.</p>
                </li>
                <li><h2>  Adventure Sports</h2>
                    <p>If you are going for an adventure tour, then make sure that your travel insurance policy gives you cover for all such activities. Normal travel insurance policies don’t offer any cover for adventure sports. Such sports are often mentioned in their policy document as well. So if you are going on a trip where you will get involved in such adventure sports and activities, you must reveal it to your insurer. Insurance Companies in UAE may offer you a different travel insurance policy with an extra premium that will cover your adventure sports and activities.</p>
                    <p>Scuba diving, rock climbing, sky diving, paragliding and parachuting, jet skiing, motor-biking, snowboarding, and surfing are some of the adventure sports activities which are often not covered by a common travel insurance policy.</p></li>
                <li>
                    <h2>Risky Behaviour</h2>
                    <p>Travel insurance doesn’t accept any claim or doesn’t offer cover if you put yourself at risk deliberately. No claim is offered in case of injury or loss if you were under the influence of drug or alcohol at the time of loss or damage. Similarly, if you lost your baggage which you left unattended, no cover or claim would be offered by the insurer in such case. Also, the most important point that insurance companies don’t cover the loss of cash because keeping excess cash while you have credit cards is something unnecessary.</p>
                </li>
                <li>
                    <h2>Outbreaks of Disease</h2>
                    <p>If there’s an outbreak of any disease in the country you are travelling to then, this will not be covered by any travel insurance policy.</p>
                </li>
                <li>
                    <h2>Civil and Political Unrest, War and Terrorism</h2>
                    <p>Certain governments recommend not to travel to the certain country for the sake of security. Such recommendation may be timely or permanent, and the reason can be civil unrest, war or terrorism. If you travel to such a country, you will not have any cover because travel insurance company will not accept any claim in case of loss or damage. If you are planning a trip, then it’s good to research well about your destination so that you may get complete cover during your travel.</p>
                </li>
                <li>
                    <h2>Personal Belongings and Luggage</h2>
                    <p>As mentioned above various times that you must read the policy document carefully. Every insurance policy gives you a limited cover. If you have some precious things in your personal belongings like jewellery, expensive watches, laptops, and smartphones, then the cover may be limited to a certain amount. Make sure you are aware of this fact before you travel or while you purchase the travel insurance.</p>
                </li>
                <li>
                    <h2>Over Age</h2>
                    <p>Most of the insurance companies give cover to the certain age limit. Overaged persons don’t get coverage in most of travel insurance policies, but there is some insurer which offers special insurance policies for senior travellers.</p>
                </li>
            </ul>
            <h1>How Premiums are Calculated?</h1>
            <p>Insurance premium, the cost of insurance, is based upon various factors. The travel insurance premium amount depends upon the type of coverage you want, your medical health, your age, the type of your trip, duration of your trip, and the destination country.</p>
            <p>Travel insurance premium is calculated, mainly, keeping in view the following factors:</p>
            <ul>
                <li>Duration of your trip</li>
                <li>Your age at the time of travel</li>
                <li>Type of your trip, i.e., whether it’s a business trip or adventure tour</li>
                <li>Type and amount of cover you want, i.e., single person or group or family cover</li>
                <li>Your travel destinations</li>
                <li>Your health condition</li>
                <p>Insurance companies may consider other factors as well, but the factors mentioned above are the main factors which determine the amount of premium you are going to pay to the insurance company to get the require cover during your travel. For more details & information you can contact OD Travels .</p>
            </ul>

        </div>
    </div>





</section>
</div>

@endsection