@extends('layouts.app')

@section('content')

<div class="super_container">
<div class="home-1">

    <img src="{{asset('images/onarrival.jpg')}}">
    {{--<div class="rainbow-div"><h1 class="rainbow">ON ARRIVAL COUNTRIES</h1></div>--}}

    {{--<h1> ON ARRIVAL COUNTRIES</h1>--}}

</div>
<section id="flags">
    <div class="container">
        <div class="row">
            @foreach($arrival_countries as $arrival_countrie)
            <article class="post post-image col-xs-12 col-sm-6 col-md-3 dark-panel- archive-column  post-3447 page type-page status-publish has-post-thumbnail hentry">
                <!-- used get_the_content() instead of the_content() since
                we don't want wordpress to add any content divs but only output
                the content
                -->
                <div class="panel panel-default panel-link  body-fixed  dark-panel ">
                    <div class="panel-body link-img visa-wrapper">
                        <div class="img-container">
                            <a href="#" target="_self">
                                <img width="1000" height="600" src="{{$arrival_countrie->countries->avatar}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" sizes="(max-width: 1000px) 100vw, 1000px">        </a>
                            <div class="overlay">
                                <div class="text">
                                    <a href="#" target="_self">
                                        {{$arrival_countrie->countries->name}}
                                    </a>
                                </div>
                            </div>
                        <p style="color: #000; text-align: center; font-size: 18px; padding: 5px;">{{$arrival_countrie->countries->name}}</p>

                        </div>

                    </div>
                </div>
            </article>
            @endforeach
        </div>


    </div>



</section>
</div>

@endsection