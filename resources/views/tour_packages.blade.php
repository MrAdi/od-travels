@extends('layouts.app')

@section('content')

<div class="home-1">

    <img src="{{asset('images/tour.jpg')}}">

    {{--<img src="{{asset($home_sliders->avatar)}}">--}}
    {{--<div class="rainbow-div"><h1 class="rainbow">Tour</h1></div>--}}


</div>

<section id="latest-pkg">
    <div class="container">
        <p>Australia is a land of dreams. It is the world's smallest continent and the largest island. Australia is a land of spectacular beauty and staggering contrast. Along the coast, tourists can explore vibrant multicultural places, trek through ancient rainforests and safari across vast sand islands and dive the Great Barrier Reef. Enjoy the main highlights of Sydney, Melbourne, and Brisbane with our best and exciting tour packages and make your vacations worth going.</p>
        <h1>Latest Packages</h1>
        <div class="row">

            @foreach($tours as $tour)
            <div class="col-md-3">
                <div class="contry-c">
                    <div class="img-container">
                        <a href="{{ route('tour_detail',$tour->id) }}">
                            <img src="{{asset($tour->cover_url)}}" style="height: 159px; width: 253px;"></a>
                        <div class="overlay">
                            <div class="text">
                                <a href="{{ route('tour_detail',$tour->id) }}" target="_self">
                                    {{$tour->heading}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <p><span><b>Stay:</b> {{$tour->stay}}</span></p>
                    <p><b>Hotel:</b> {{$tour->hotel}}</p>
                    <p><b>Ticket:</b> {{$tour->ticket}}</p>
                    <p><b>Visa:</b> {{$tour->visa}}</p>
                    <p><b>Price:</b> {{$tour->price}}</p>
                    <button class="btn btn-primary"><a href="{{ route('tour_detail',$tour->id) }}"> Further Details</a></button>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@include('layouts.videos')

@endsection