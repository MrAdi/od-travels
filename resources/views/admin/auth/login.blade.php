 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

    <title>OD Travels</title>

    <link rel="stylesheet" href="{{ asset('admin/lib/fontawesome/css/font-awesome.css') }}">

    <link rel="stylesheet" href="{{ asset('css/app.css')  }}">
    <link rel="stylesheet" href="{{ asset('admin/css/quirk.css')  }}">

    <script src="{{ asset('admin/lib/modernizr/modernizr.js')  }}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ asset('admin/lib/html5shiv/html5shiv.js') }}"></script>
    <script src="{{ asset('admin/lib/respond/respond.src.js') }}"></script>
    <![endif]-->
    <style>
        .input-group .invalid-feedback{
            display: inline-block;
        }
    </style>
</head>

<body class="signwrapper">

<div class="sign-overlay"></div>
<div class="signpanel"></div>

<div class="panel signin">
    <div class="panel-heading">
        <h1>OD Bytes</h1>
        <h4 class="panel-title">Welcome! Please signin.</h4>
    </div>
    <div class="panel-body">

        <form action="{{ route('admin.login') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group mb10">
                <div class="input-group">
                    <span class="input-group-addon "><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Enter Username" autofocus>
                </div>
                @if ($errors->has('email'))
                    <div class="input-group ">
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    </div>
                @endif

            </div>
            <div class="form-group nomargin">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Enter Password">
                </div>
                @if ($errors->has('password'))
                    <div class="input-group ">
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    </div>
                @endif
            </div>
            {{--<div>--}}
                {{--<a href="" class="forgot">Forgot password?</a>--}}
            {{--</div>--}}
            <div class="form-group" style="margin-top: 20px">
                <button class="btn btn-success btn-quirk btn-block">Sign In</button>
            </div>
        </form>
        <hr class="invisible">
        {{--<div class="form-group">--}}
            {{--<a href="admin/signup" class="btn btn-default btn-quirk btn-stroke btn-stroke-thin btn-block btn-sign">Not a member? Sign up now!</a>--}}
        {{--</div>--}}
    </div>
</div><!-- panel -->

</body>
</html>
