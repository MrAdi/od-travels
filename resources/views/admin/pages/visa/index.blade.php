@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->


                    <div class="container">
                        <h2>Visa Detail</h2>

                        <div class="table-responsive">

                        <table class="table table-responsive table-hover">
                            <thead>
                            <tr>
                                <th>Country</th>
                                <th>Visa Name</th>
                                <th>Banner Heading</th>
                                <th>Picture</th>
                                <th>Description</th>
                                <th>Processing Time</th>
                                <th>Update</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($adis as $adi)
                            <tr>
                                <td>{{$adi->country_id}}</td>
                                <td>{{$adi->name}}</td>
                                <td>{{$adi->banner_heading}}</td>
                                <td><img src="{{ asset($adi->banner_url)}}" width="100px"></td>
                                <td>{{$adi->description}}</td>
                                <td>{{$adi->processing_time}}</td>
                                <td>
                                    <a href="{{route('admin.visa.edit',$adi->id)}}" class="btn btn-info" >Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.visa.destroy', $adi->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-info" name="delete" style="background-color: #d62728">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>



            </div>
        </div>
    </div>

@endsection