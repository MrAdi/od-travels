@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Visa</h4>
                                </div>
                                <form method="post" action="{{route('admin.visa.index')}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Select Country</h1><br />
                                                <select id="select2" name="country_id" class="form-control" style="width: 100%" data-placeholder="Select Country" multiple>
                                                    @foreach($countrys as $country)
                                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Visa Name</h1><br />
                                                <input type="text" name="name" placeholder="Visa Name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Banner Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <input type='file' name="banner_url" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Banner Heading</h1><br />
                                                <input type="text" name="banner_heading" placeholder="Banner Heading" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <h1 class="panel-title">Description</h1><br />
                                                <input type="text" name="description" placeholder="Description" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Processing Time</h1><br />
                                                <input type="text" name="processing_time" placeholder="Processing Time" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <input class="btn btn-primary" name="submit" type="submit" value="Create" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--<script src="{{ asset('admin/js/style.js') }}"></script>--}}
