@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')
    {{($visa->banner_url) }}
    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Visa</h4>
                                </div>
                                @if($errors->all())
                                    <div class="alert alert-danget">
                                        @foreach($errors->all() as $error)

                                            <li>{{$error}}</li>

                                        @endforeach
                                    </div>
                                @endif


                                <form method="post" action="{{route('admin.visa.update',$visa->id)}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @method('patch')
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Country</h1><br />
                                                <select id="select2" name="country_id" class="form-control" style="width: 100%" data-placeholder="" multiple>
                                                    @foreach($countrys as $country)
                                                        <option @if($visa->country_id == $country->id)selected @endif value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Visa Name</h1><br />
                                                <input type="text" value="{{($visa->name)}}" name="name" placeholder="Banner Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                        <img src="{{ asset($visa->banner_url) }}" width="100px">
                                                        </div><br />
                                                        <div class="avatar-edit">
                                                            <input type='file' value="{{($visa->banner_url)}}" name="banner_url" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <h1 class="panel-title">Banner Heading</h1><br />
                                                <input type="text" value="{{($visa->banner_heading)}}" name="banner_heading" placeholder="Banner Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Description</h1><br />
                                                <input type="text" value="{{($visa->description)}}" name="description" placeholder="Description" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Processing Time</h1><br />
                                                <input type="text" value="{{($visa->processing_time)}}" name="processing_time" placeholder="Processing Time" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <input class="btn btn-primary" name="Update" type="submit" value="Update" style="margin-left:270px ; width:100px">
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--<script src="{{ asset('admin/js/style.js') }}"></script>--}}
