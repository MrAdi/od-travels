@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->




                <div class="container">
                    <h2>Offers</h2>



                    <table class="table table-responsive table-hover">
                        <thead>
                        <tr>

                            <th>Heading</th>
                            <th>Picture</th>
                            <th>Price</th>
                            <th>Offers Text</th>
                            <th>Update</th>
                            <th>Delete</th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($adis as $adi)
                            <tr>
                                <td>{{$adi->heading}}</td>
                                <td><img src="{{ asset($adi->avatar) }}" width="100px"></td>
                                <td>{{$adi->price}}</td>
                                <td>{{$adi->text}}</td>
                                <td>
                                    <a href="{{route('admin.offers.edit',$adi->id)}}" class="btn btn-info">Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.offers.destroy', $adi->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-info" name="delete">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>



            </div>
        </div>
    </div>

@endsection