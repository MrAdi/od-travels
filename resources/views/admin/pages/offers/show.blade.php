@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">About Us</h4>
                                </div>
                                <form>
                                    <div class="form">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">MOBILE #</h1><br />
                                                <input type="text" placeholder="Mobile #" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">ADDRESS</h1><br />
                                                <input type="text" placeholder="Address" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">GMAIL</h1><br />
                                                <input type="text" placeholder="Gmail" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">FACEBOOK</h1><br />
                                                <input type="text" placeholder="Facebook" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">FOOTER TEXT</h1><br />
                                                <input type="text" placeholder="Footer text" class="form-control">
                                            </div>
                                            <div class="form-group">

                                                <input class="btn btn-primary" type="submit" value="Next" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection