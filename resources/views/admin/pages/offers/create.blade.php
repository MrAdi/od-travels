@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Offers</h4>
                                </div>
                                <form method="post" action="{{route('admin.offers.index')}}" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Heading</h1><br />
                                                <input type="text" name="heading" placeholder="Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <input type='file' name="avatar" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Price</h1><br />
                                                <input type="text" name="price" placeholder="Price" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Offers Text</h1><br />
                                                <input type="text" name="text" placeholder="Text" class="form-control">
                                            </div>



                                            <div class="form-group">
                                                <input class="btn btn-primary" type="submit" value="Create" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection