@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Testimonials</h4>
                                </div>
                                @if($errors->all())
                                    <div class="alert alert-danget">
                                        @foreach($errors->all() as $error)

                                            <li>{{$error}}</li>

                                        @endforeach
                                    </div>
                                @endif
                                <form method="post" action="{{route('admin.testimonials.update',$testimonial->id)}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @method('patch')
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Name</h1><br />
                                                <input type="text" value="{{$testimonial->name}}" name="name" placeholder="Name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Title</h1><br />
                                                <input type="text" value="{{$testimonial->title}}" name="title" placeholder="Title" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <h1 class="panel-title">Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <img src="{{ asset($testimonial->avatar) }}" width="100px">
                                                        </div><br />
                                                        <div class="avatar-edit">
                                                            <input type='file' value="{{($testimonial->avatar)}}" name="avatar" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <h1 class="panel-title">Text</h1><br />
                                                <input type="text" value="{{$testimonial->text}}" name="text" placeholder="Text" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Date</h1><br />
                                            <div class="input-group">
                                                <input type="text" name="date" value="{{$testimonial->date}}" class="form-control" placeholder="mm/dd/yyyy" id="datepicker">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                            </div>

                                            <div class="form-group">
                                                <input class="btn btn-primary" name="Update" type="submit" value="Update" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>
                                    </div>
                                </form>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')


    <script src="{{ asset('admin/lib/jquery/jquery.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-ui/jquery-ui.js')}}"></script>
    <script src="{{ asset('admin/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-autosize/autosize.js')}}"></script>
    <script src="{{ asset('admin/lib/select2/select2.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
    <script src="{{ asset('admin/lib/timepicker/jquery.timepicker.js')}}"></script>
    <script src="{{ asset('admin/lib/dropzone/dropzone.js')}}"></script>
    <script src="{{ asset('admin/lib/bootstrapcolorpicker/js/bootstrap-colorpicker.js')}}"></script>
    {{--<script src="{{ asset('admin/js/quirk.js')}}"></script>--}}

    <script>
        $(function() {


            // Input Masks
            $("#date").mask("99/99/9999");
            $("#phone").mask("(999) 999-9999");
            $("#ssn").mask("999-99-9999");

            // Date Picker
            $('#datepicker').datepicker({
                format:"YYYY/MM",
                startView:"year",
                minViewMode:"months"

            });
            $('#datepicker-inline').datepicker();
            $('#datepicker-multiple').datepicker({ numberOfMonths: 2 });

            // Time Picker
            $('#tpBasic').timepicker();
            $('#tp2').timepicker({'scrollDefault': 'now'});
            $('#tp3').timepicker();

            $('#setTimeButton').on('click', function (){
                $('#tp3').timepicker('setTime', new Date());
            });



        });
    </script>

@endsection