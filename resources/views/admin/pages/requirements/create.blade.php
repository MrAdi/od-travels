@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Requirments</h4>
                                </div>
                                <form method="post" action="{{route('admin.requirments.index')}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Name</h1><br />
                                                <input type="text" name="name" placeholder="Name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Status</h1><br />
                                                <select id="select2" name="status" class="form-control" style="width: 100%" data-placeholder="Status">

                                                        <option value="0">Disable</option>
                                                        <option value="1">Enable</option>

                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <input class="btn btn-primary" name="submit" type="submit" value="Create" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--<script src="{{ asset('admin/js/style.js') }}"></script>--}}
