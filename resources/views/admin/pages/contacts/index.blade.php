@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->




                <div class="container">
                    <h2>CONTACT INFO</h2>

                    <div class="table-responsive">

                    <table class="table table-responsive table-hover">
                        <thead>
                        <tr>

                            <th>MOBILE #</th>
                            <th>ADDRESS</th>
                            <th>Email</th>
                            <th>FACEBOOK</th>
                            <th>TWITTER</th>
                            <th>LINKEDIN</th>
                            <th>FOOTER TEXT</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($adis as $adi)
                            <tr>
                                <td>{{$adi->mobile}}</td>
                                <td>{{$adi->address}}</td>
                                <td>{{$adi->email}}</td>
                                <td>{{$adi->facebook}}</td>
                                <td>{{$adi->twitter}}</td>
                                <td>{{$adi->linkedin}}</td>
                                <td>{{$adi->text}}</td>
                                <td>
                                    <a href="{{route('admin.contacts.edit',$adi->id)}}" class="btn btn-info">Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.contacts.destroy', $adi->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-info" name="delete">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>



            </div>
        </div>
    </div>

@endsection