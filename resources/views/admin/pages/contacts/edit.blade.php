@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Contact Us</h4>
                                </div>
                                @if($errors->all())

                                    <div class="alert alert-danget">
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </div>
                                @endif
                                <form method="post" action="{{route('admin.contacts.update',$contact->id)}}">
                                    <div class="form">
                                        @method('patch')
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">MOBILE #</h1><br />
                                                <input type="text" value="{{$contact->mobile}}" name="mobile" placeholder="Mobile #" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">ADDRESS</h1><br />
                                                <input type="text" value="{{$contact->address}}" name="address" placeholder="Address" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">EMAIL</h1><br />
                                                <input type="text" value="{{$contact->email}}" name="email" placeholder="Gmail" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">FACEBOOK</h1><br />
                                                <input type="text" value="{{$contact->facebook}}" name="facebook" placeholder="Facebook" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">TWITTER</h1><br />
                                                <input type="text" value="{{$contact->twitter}}" name="twitter" placeholder="Twitter" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">LINKEDIN</h1><br />
                                                <input type="text" value="{{$contact->linkedin}}" name="linkedin" placeholder="Linkedin" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">FOOTER TEXT</h1><br />
                                                <input type="text" value="{{$contact->text}}" name="text" placeholder="Footer text" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input class="btn btn-primary" name="submit" type="submit" value="UPDATE" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection