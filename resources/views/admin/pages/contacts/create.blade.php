@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">CONTACT INFO</h4>
                                </div>
                                <form method="post" action="{{route('admin.contacts.index')}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">MOBILE #</h1><br />
                                                <input type="text" name="mobile" placeholder="Mobile #" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">ADDRESS</h1><br />
                                                <input type="text" name="address" placeholder="Address" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">EMAIL</h1><br />
                                                <input type="text" name="email" placeholder="Email" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">FACEBOOK</h1><br />
                                                <input type="text" name="facebook" placeholder="Facebook" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">TWITTER</h1><br />
                                                <input type="text" name="twitter" placeholder="Twitter" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">LINKEDIN</h1><br />
                                                <input type="text" name="linkedin" placeholder="Linkedin" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">FOOTER TEXT</h1><br />
                                                <input type="text" name="text" placeholder="Footer text" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input class="btn btn-primary" name="submit" type="submit" value="CREATE" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection