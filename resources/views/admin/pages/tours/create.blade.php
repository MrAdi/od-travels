@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    <link rel="stylesheet" href="{{asset('admin/lib/fontawesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('admin/lib/weather-icons/css/weather-icons.css')}}">
    <link rel="stylesheet" href="{{asset('admin/lib/jquery-toggles/toggles-full.css')}}">
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}
    <link rel="stylesheet" href="{{asset('admin/lib/summernote/summernote.css')}}">


    <link rel="stylesheet" href="{{asset('admin/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.css')}}">

    <link href="{{asset('admin/css/quirk.css')}}" rel="stylesheet">

    <script src="{{asset('admin/lib/modernizr/modernizr.js')}}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>-->
  <script src="{{asset('admin/lib/html5shiv/html5shiv.js')}}"></script>
  <script src="{{asset('admin/lib/respond/respond.src.js')}}"></script>

@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Tours Detail</h4>
                                </div>
                                <form method="post" action="{{route('admin.tours.store')}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Heading</h1><br />
                                                <input type="text" name="heading" placeholder="Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <input type='file' name="cover_url" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Price</h1><br />
                                                <input type="text" name="price" placeholder="About Us Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Description</h1><br />
                                                <input type="text" name="description" placeholder="Text" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <h1 class="panel-title">Package Includes</h1><br />
                                                <textarea id="summernote1" name="pkg_includes" placeholder="Package Includes" class="form-control" rows="10"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Package Exclusive</h1><br />
                                                <div class="panel">
                                                    <textarea id="summernote2" name="pkg_exclusive" placeholder="Package Exclusive" class="form-control" rows="10"></textarea>

                                                    {{--<div class="panel-heading">--}}
                                                        {{--<h4 class="panel-title">Summernote</h4>--}}
                                                        {{--<p>Super Simple WYSIWYG Editor on Bootstrap. <a href="http://summernote.org/" target="_blank">http://summernote.org/</a></p>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="panel-body">--}}
                                                        {{--<div id="summernote">Hello Summernote</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Package Title</h1><br />
                                                <input type="text" name="package_title" placeholder="Package Title" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Departure Country</h1><br />
                                                <input type="text" name="departure_country" placeholder="Departure Country" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Departure City</h1><br />
                                                <input type="text" name="departure_city" placeholder="Departure City" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Min Allowed</h1><br />
                                                <input type="text" name="min_allowed" placeholder="Min Allowed" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Ticket</h1><br />
                                                <input type="text" name="ticket" placeholder="Ticket" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Visa</h1><br />
                                                <input type="text" name="visa" placeholder="Visa" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Insurance</h1><br />
                                                <input type="text" name="insurance" placeholder="Insurance" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Stay</h1><br />
                                                <input type="text" name="stay" placeholder="Stay" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Hotel</h1><br />
                                                <input type="text" name="hotel" placeholder="Hotel" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Company</h1><br />
                                                <input type="text" name="company" placeholder="Company" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Rate Mentioned</h1><br />
                                                <input type="text" name="rate_mentioned" placeholder="Rate Mentioned" class="form-control">
                                            </div>
                                            <div class="panel-body">
                                                <h1 class="panel-title">Valid Till</h1><br />
                                                <div class="input-group">
                                                    <input type="text" name="valid_till" class="form-control" placeholder="yyyy/mm/dd" id="datepicker1">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div><div class="panel-body">
                                                <h1 class="panel-title">Posted On</h1><br />
                                                <div class="input-group">
                                                    <input type="text" name="posted_on" class="form-control" placeholder="yyyy/mm/dd" id="datepicker2">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <input class="btn btn-primary" name="submit" type="submit" value="Create" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection
@section('script')


    <script src="{{ asset('admin/lib/jquery/jquery.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-ui/jquery-ui.js')}}"></script>
    <script src="{{ asset('admin/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-autosize/autosize.js')}}"></script>
    <script src="{{ asset('admin/lib/select2/select2.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.js')}}"></script>
    <script src="{{ asset('admin/lib/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
    <script src="{{ asset('admin/lib/timepicker/jquery.timepicker.js')}}"></script>
    <script src="{{ asset('admin/lib/dropzone/dropzone.js')}}"></script>
    <script src="{{ asset('admin/lib/bootstrapcolorpicker/js/bootstrap-colorpicker.js')}}"></script>
    {{--<script src="{{ asset('admin/js/quirk.js')}}"></script>--}}
    <script src="{{asset('admin/lib/wysihtml5x/wysihtml5x.js')}}"></script>
    <script src="{{asset('admin/lib/wysihtml5x/wysihtml5x-toolbar.js')}}"></script>
    <script src="{{asset('admin/lib/handlebars/handlebars.js')}}"></script>
    <script src="{{asset('admin/lib/summernote/summernote.js')}}"></script>
    <script src="{{asset('admin/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.all.js')}}"></script>


    <script>
        $(function() {


            // Input Masks
            $("#date").mask("99/99/9999");
            $("#phone").mask("(999) 999-9999");
            $("#ssn").mask("999-99-9999");

            // Date Picker1
            $('#datepicker1').datepicker({
                format:"YYYY/MM",
                startView:"year",
                minViewMode:"months"

            });
            $('#datepicker-inline').datepicker();
            $('#datepicker-multiple').datepicker({ numberOfMonths: 2 });

            // Date Picker2
            $('#datepicker2').datepicker({
                format:"YYYY/MM",
                startView:"year",
                minViewMode:"months"

            });
            $('#datepicker-inline').datepicker();
            $('#datepicker-multiple').datepicker({ numberOfMonths: 2 });

            // Time Picker
            $('#tpBasic').timepicker();
            $('#tp2').timepicker({'scrollDefault': 'now'});
            $('#tp3').timepicker();

            $('#setTimeButton').on('click', function (){
                $('#tp3').timepicker('setTime', new Date());
            });



        });
    </script>
    <script>

        $(document).ready(function(){


            // Summernote
            $('#summernote1').summernote({
                height: 200
            });
            $('#summernote2').summernote({
                height: 200
            });

        });
    </script>

@endsection

@section('script')

<script src="{{asset('admin/lib/wysihtml5x/wysihtml5x.js')}}"></script>
<script src="{{asset('admin/lib/wysihtml5x/wysihtml5x-toolbar.js')}}"></script>
<script src="{{asset('admin/lib/handlebars/handlebars.js')}}"></script>
<script src="{{asset('admin/lib/summernote/summernote.js')}}"></script>
<script src="{{asset('admin/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.all.js')}}"></script>



<script>

    $(document).ready(function(){


        // Summernote
        $('#summernote1').summernote({
            height: 200
        });
        $('#summernote2').summernote({
            height: 200
        });

    });
</script>
@endsection