@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')
    {{($tour->avatar) }}
    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Tours</h4>
                                </div>
                                @if($errors->all())
                                    <div class="alert alert-danget">
                                        @foreach($errors->all() as $error)

                                            <li>{{$error}}</li>

                                        @endforeach
                                    </div>
                                @endif

                                <form method="post" action="{{route('admin.tours.update',$tour->id)}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @method('patch')
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Heading</h1><br />
                                                <input type="text" name="heading" value="{{$tour->heading}}" placeholder="Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <img src="{{ asset($tour->cover_url) }}" width="100px">
                                                        </div><br />
                                                        <div class="avatar-edit">
                                                            <input type='file' value="{{$tour->cover_url}}" name="cover_url" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Price</h1><br />
                                                <input type="text" value="{{$tour->price}}" name="price" placeholder="About Us Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Description</h1><br />
                                                <input type="text" value="{{$tour->description}}" name="description" placeholder="Text" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <h1 class="panel-title">Package Includes</h1><br />
                                                <textarea id="wysiwyg"  name="pkg_includes" placeholder="Package Includes" class="form-control" rows="10">{{$tour->pkg_includes}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Package Exclusive</h1><br />
                                                <textarea id="wysiwyg" name="pkg_exclusive" placeholder="Package Exclusive" class="form-control" rows="10">{{$tour->pkg_exclusive}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Package Title</h1><br />
                                                <input type="text" value="{{$tour->package_title}}" name="package_title" placeholder="Package Title" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Departure Country</h1><br />
                                                <input type="text" value="{{$tour->departure_country}}" name="departure_country" placeholder="Departure Country" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Departure City</h1><br />
                                                <input type="text" value="{{$tour->departure_city}}" name="departure_city" placeholder="Departure City" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Min Allowed</h1><br />
                                                <input type="text" value="{{$tour->min_allowed}}" name="min_allowed" placeholder="Min Allowed" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Ticket</h1><br />
                                                <input type="text" value="{{$tour->ticket}}" name="ticket" placeholder="Ticket" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Visa</h1><br />
                                                <input type="text" value="{{$tour->visa}}" name="visa" placeholder="Visa" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Insurance</h1><br />
                                                <input type="text" value="{{$tour->insurance}}" name="insurance" placeholder="Insurance" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Stay</h1><br />
                                                <input type="text" value="{{$tour->stay}}" name="stay" placeholder="Stay" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Hotel</h1><br />
                                                <input type="text" value="{{$tour->hotel}}" name="hotel" placeholder="Hotel" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Company</h1><br />
                                                <input type="text" value="{{$tour->company}}" name="company" placeholder="Company" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Rate Mentioned</h1><br />
                                                <input type="text" value="{{$tour->rate_mentioned}}" name="rate_mentioned" placeholder="Rate Mentioned" class="form-control">
                                            </div>
                                            <div class="panel-body">
                                                <h1 class="panel-title">Valid Till</h1><br />
                                                <div class="input-group">
                                                    <input type="text" value="{{$tour->valid_till}}" name="valid_till" class="form-control" placeholder="yyyy/mm/dd" id="datepicker">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div><div class="panel-body">
                                                <h1 class="panel-title">Posted On</h1><br />
                                                <div class="input-group">
                                                    <input type="text" value="{{$tour->posted_on}}" name="posted_on" class="form-control" placeholder="yyyy/mm/dd" id="datepicker">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <input class="btn btn-primary" name="submit" type="submit" value="Update" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

