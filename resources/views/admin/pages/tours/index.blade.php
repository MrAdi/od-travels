@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->

                <div class="container">
                        <h2>Tours Detail</h2>

                    <div class="table-responsive">

                        <table class="table table-responsive table-hover">
                            <thead>
                            <tr>
                                <th>Heading</th>
                                <th>Picture</th>
                                <th>Price</th>
                                <th>Description</th>
                                <th>package Includes</th>
                                <th>package Exclusive</th>
                                <th>Package Title</th>
                                <th>Departure Country</th>
                                <th>Departure City</th>
                                <th>Minimum Allowed</th>
                                <th>Ticket</th>
                                <th>Visa</th>
                                <th>Insurance</th>
                                <th>stay</th>
                                <th>Hotel</th>
                                <th>Company</th>
                                <th>Rate_mentioned</th>
                                <th>Valid_till</th>
                                <th>Posted_on</th>
                                <th>Update</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($adis as $adi)
                            <tr>
                                <td>{{$adi->heading}}</td>
                                <td><img src="{{ asset($adi->cover_url) }}" width="100px"></td>
                                <td>{{$adi->price}}</td>
                                <td>{{$adi->description}}</td>
                                <td>{{$adi->pkg_includes}}</td>
                                <td>{{$adi->pkg_exclusive}}</td>
                                <td>{{$adi->package_title}}</td>
                                <td>{{$adi->departure_country}}</td>
                                <td>{{$adi->departure_city}}</td>
                                <td>{{$adi->min_allowed}}</td>
                                <td>{{$adi->ticket}}</td>
                                <td>{{$adi->visa}}</td>
                                <td>{{$adi->insurance}}</td>
                                <td>{{$adi->stay}}</td>
                                <td>{{$adi->hotel}}</td>
                                <td>{{$adi->company}}</td>
                                <td>{{$adi->rate_mentioned}}</td>
                                <td>{{$adi->valid_till}}</td>
                                <td>{{$adi->posted_on}}</td>
                                <td>
                                    <a href="{{route('admin.tours.edit',$adi->id)}}" class="btn btn-info" >Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.tours.destroy', $adi->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-info" name="delete" style="background-color: #d62728">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>



            </div>
        </div>
    </div>

@endsection