@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')
    {{($tours_slider->url) }}
    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Tour Slider</h4>
                                </div>
                                @if($errors->all())
                                    <div class="alert alert-danget">
                                        @foreach($errors->all() as $error)

                                            <li>{{$error}}</li>

                                        @endforeach
                                    </div>
                                @endif

                                <form method="post" action="{{route('admin.tours_slider.update',['tours_slider'=>$tours_slider->id])}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @method('patch')
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Select Tour</h1><br />
                                                <select id="select2"  name="tours_id" class="form-control" style="width: 100%" data-placeholder="" multiple>
                                                    @foreach($tours as $tour)
                                                        <option @if($tours_slider->tours_id == $tour->id)selected @endif value="{{$tour->id}}">{{$tour->heading}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Heading</h1><br />
                                                <input type="text" value="{{($tours_slider->heading)}}" name="heading" placeholder="Heading" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Text Heading</h1><br />
                                                <input type="text" value="{{($tours_slider->text)}}" name="text" placeholder="Text" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input class="btn btn-primary" name="Update" type="submit" value="Update" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--<script src="{{ asset('admin/js/style.js') }}"></script>--}}
