@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')
    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Visa</h4>
                                </div>
                                @if($errors->all())
                                    <div class="alert alert-danget">
                                        @foreach($errors->all() as $error)

                                            <li>{{$error}}</li>

                                        @endforeach
                                    </div>
                                @endif


                                <form method="post" action="{{route('admin.umrah.update',$umrah->id)}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @method('patch')
                                        @csrf
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h1 class="panel-title">Banner Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <img src="{{ asset($umrah->cover_url) }}" width="100px">
                                                        </div><br />
                                                        <div class="avatar-edit">
                                                            <input type='file' value="{{($umrah->cover_url)}}" name="cover_url" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title"> Name</h1><br />
                                                <input type="text" value="{{($umrah->name)}}" name="name" placeholder="name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title"> number of days</h1><br />
                                                <input type="text" value="{{($umrah->days)}}" name="days" placeholder="days" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Picture</h1><br />
                                                <div class="container">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                        <img src="{{ asset($umrah->url) }}" width="100px">
                                                        </div><br />
                                                        <div class="avatar-edit">
                                                            <input type='file' value="{{($umrah->url)}}" name="url" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <h1 class="panel-title">Hotel</h1><br />
                                                <input type="text" value="{{($umrah->hotel)}}" name="hotel" placeholder="hotel" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Visa</h1><br />
                                                <input type="text" value="{{($umrah->visa)}}" name="visa" placeholder="visa" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Ticket</h1><br />
                                                <input type="text" value="{{($umrah->ticket)}}" name="ticket" placeholder="Ticket" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Price</h1><br />
                                                <input type="text" value="{{($umrah->price)}}" name="price" placeholder="Price" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <input class="btn btn-primary" name="Update" type="submit" value="Update" style="margin-left:270px ; width:100px">
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--<script src="{{ asset('admin/js/style.js') }}"></script>--}}
