@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->


                    <div class="container">
                        <h2>Umrah</h2>

                        <div class="table-responsive">

                        <table class="table table-responsive table-hover">
                            <thead>
                            <tr>
                                <th>Cover Picture</th>
                                <th>Name</th>
                                <th>Days</th>
                                <th>Picture</th>
                                <th>Hotel</th>
                                <th>Visa</th>
                                <th>Ticket</th>
                                <th>Price</th>
                                <th>Update</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($adis as $adi)
                            <tr>
                                <td><img src="{{ asset($adi->cover_url)}}" width="100px"></td>
                                <td>{{$adi->name}}</td>
                                <td>{{$adi->days}}</td>
                                <td><img src="{{ asset($adi->url)}}" width="100px"></td>
                                <td>{{$adi->hotel}}</td>
                                <td>{{$adi->visa}}</td>
                                <td>{{$adi->ticket}}</td>
                                <td>{{$adi->price}}</td>
                                <td>
                                    <a href="{{route('admin.umrah.edit',$adi->id)}}" class="btn btn-info" >Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.umrah.destroy', $adi->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-info" name="delete" style="background-color: #d62728">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>



            </div>
        </div>
    </div>

@endsection