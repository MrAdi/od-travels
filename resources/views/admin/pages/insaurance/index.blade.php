@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->


                {{--<li>{{$adi->heading}}</li>--}}
                {{--<li>{{$adi->avatar}}</li>--}}
                {{--<li>{{$adi->text_heading}}</li>--}}
                {{--<li>{{$adi->text}}</li>--}}


                <div class="container">
                    <h2>Insaurance Picture</h2>



                    <table class="table table-responsive table-hover">
                        <thead>
                        <tr>

                            <th>Picture</th>
                            <th>Edit</th>
                            <th>Delete</th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($adis as $adi)
                            <tr>

                                <td><img src="{{ asset($adi->picture) }}" width="100px"></td>

                                <td>
                                    <a href="{{route('admin.insaurance.edit',$adi->id)}}" class="btn btn-info">Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.insaurance.destroy', $adi->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-info" name="delete">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>



            </div>
        </div>
    </div>

@endsection