@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->


                  {{--<li>{{$adi->heading}}</li>--}}
                  {{--<li>{{$adi->avatar}}</li>--}}
                  {{--<li>{{$adi->text_heading}}</li>--}}
                  {{--<li>{{$adi->text}}</li>--}}


                    <div class="container">
                        <h2>Requirments</h2>



                        <table class="table table-responsive table-hover">
                            <thead>
                            <tr>
                                <th>Visa</th>
                                <th>Requirments</th>
                                <th>Update</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visas as $visa)
                            <tr>
                                <td>{{$visa->name}}</td>
                                {{--@foreach()--}}
                                {{--<td>{{$visa->visa_id}}</td>--}}
                                {{--@endforeach--}}
                                <td>
                                    @foreach($visa->requirments as $requirment)
                                        {{ $requirment->name }},
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{route('admin.visa_requirment.edit',$visa->id)}}" class="btn btn-info" >Edit</a>
                                </td>

                            </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>



            </div>
        </div>
    </div>

@endsection