@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')
    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Visa Requirments</h4>
                                </div>
                                @if($errors->all())
                                    <div class="alert alert-danget">
                                        @foreach($errors->all() as $error)

                                            <li>{{$error}}</li>

                                        @endforeach
                                    </div>
                                @endif

                                <form method="post" action="{{route('admin.visa_requirment.update',$visa->id)}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @method('patch')
                                        @csrf
                                        <div class="panel-body">

                                            {{--<div class="form-group">--}}
                                                {{--<h1 class="panel-title">Country</h1><br />--}}
                                                {{--<input class="form-control" readonly disabled="" value="{{ $country->name }}">--}}
                                                {{--<select id="select2" name="country_id" class="form-control" style="width: 100%" data-placeholder="" multiple>--}}
                                                    {{--@foreach($countrys as $country)--}}
                                                        {{--<option @if($visa->country_id == $country->id)selected @endif value="{{$country->id}}">{{$country->name}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                            <div class="form-group">
                                                <h1 class="panel-title">Visa</h1><br />
                                                <input class="form-control" readonly disabled="" value="{{ $visa->name }}">
                                                {{--<select id="select2" name="visa_id" class="form-control" style="width: 100%" data-placeholder="Select Country" multiple>--}}
                                                    {{--@foreach($visas as $visa)--}}
                                                        {{--<option @if($visa_requirment->visa_id == $visa->id)selected @endif value="{{$visa->id}}">{{$visa->name}}</option>--}}

                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Requirments</h1><br />
                                                <select id="select2" name="requirments[]" class="form-control" style="width: 100%" data-placeholder="" multiple>
                                                    @foreach($requirments as $requirment)
                                                        <option @if(in_array($requirment->name,$visa_requirments) !== false)selected @endif value="{{$requirment->id}}">{{$requirment->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <input class="btn btn-primary" name="Update" type="submit" value="Update" style="margin-left:270px ; width:100px">
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--<script src="{{ asset('admin/js/style.js') }}"></script>--}}
