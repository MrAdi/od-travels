@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{ asset('admin/lib/Hover/style.css') }}">--}}

@endsection
@section('content')

    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">

                        <div class="col-sm-8">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Visa Requirments</h4>
                                </div>
                                <form method="post" action="{{route('admin.visa_requirment.index')}}" enctype="multipart/form-data">
                                    <div class="form">
                                        @csrf
                                        <div class="panel-body">

                                            <div class="form-group">
                                                <h1 class="panel-title">Select Country</h1><br />
                                                <select id="country_id" name="country_id" class="form-control input-lg dynamic select2" data-dependent="visa_id" style="width: 100%" data-placeholder="Select Country">
                                                    @foreach($countrys as $country)
                                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Select Visa</h1><br />
                                                <select id="visa_id" name="visa_id" class="form-control" style="width: 100%" data-placeholder="Select Visa Category">
                                                    @foreach($visas as $visa)
                                                        <option value="{{$visa->id}}">{{$visa->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <h1 class="panel-title">Requirments</h1><br />
                                                <select id="select2" name="requirment_id[]" class="form-control" style="width: 100%" data-placeholder="Select Requirments" multiple>
                                                    @foreach($requirments as $requirment)
                                                        <option value="{{$requirment->id}}">{{$requirment->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <input class="btn btn-primary" name="submit" type="submit" value="Create" style="margin-left:270px ; width:100px">
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--{{ csrf_field() }}--}}
@endsection
@section('script')
    <script>
        {{--$(document).ready(function(){--}}
$(document).on('change','#country_id',function(){
            if($(this).val() != '')
            {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"/admin/visa_requirment/fetch/" + value,
                    method:"POST",
                    data:{select:select, value:value, _token:_token, dependent:dependent},
                    success:function(result)
                    {
                        $('#'+dependent).html(result);
                    }

                })
            }
        });

        {{--$('#country').change(function(){--}}
        {{--$('#visa').val('');--}}
        {{--});--}}


        {{--});--}}
    </script>
@endsection





{{--<script src="{{ asset('admin/js/style.js') }}"></script>--}}
