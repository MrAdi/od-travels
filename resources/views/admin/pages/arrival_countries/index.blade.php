@extends('admin.layouts.master')
@section('style')
    <style>
        .profile-wrapper{
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')
    <?php $active ='arrival_countries'; ?>


    <div class="row profile-wrapper">

        <div class="col-md-6 col-lg-8 profile-right">
            <div id="about" class="profile-right-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-line">
                    @include('admin.layouts.tab')
                </ul>

                <!-- Tab panes -->


                    <div class="container">
                        <h2>Arrival Countries</h2>

                        <div class="table-responsive">

                        <table class="table table-responsive table-hover">
                            <thead>
                            <tr>
                                <th>Country Name</th>
                                {{--<th>Update</th>--}}
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($adis as $adi)
                            <tr>
                                <td>{{$adi->countries->name}}</td>
                                {{--{{dd($adi->country_id)}}--}}

                                {{--<td>--}}
                                    {{--<a href="{{route('admin.arrival_countries.edit',$adi->id)}}" class="btn btn-info" >Edit</a>--}}
                                {{--</td>--}}
                                <td>
                                    <form action="{{route('admin.arrival_countries.destroy',$adi->id)}}" method="post">
                                        @csrf
                                        @method('delete')

                                        <button type="submit" class="btn btn-info" name="delete" style="background-color: #d62728">
                                            Delete
                                        </button>
                                    </form>


                                    {{--<form action="{{route('admin.arrival_countries.destroy', $adi->id)}}" method="post">--}}
                                        {{--@method('delete')--}}
                                        {{--@csrf--}}
                                        {{--<button type="submit" class="btn btn-info" name="delete" style="background-color: #d62728">--}}
                                            {{--Delete--}}
                                        {{--</button>--}}
                                    {{--</form>--}}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>



            </div>
        </div>
    </div>

@endsection