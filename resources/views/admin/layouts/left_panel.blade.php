<div class="media leftpanel-profile">
    <div class="media-left">
        <a href="#">
            <img src="{{ asset('admin/images/photos/loggeduser.png') }}" alt="" class="media-object img-circle">
        </a>
    </div>
    <div class="media-body">
        <h4 class="media-heading">Mr Adi<a data-toggle="collapse" data-target="#loguserinfo" class="pull-right"><i class="fa fa-angle-down"></i></a></h4>
        <span>Software Engineer</span>
    </div>
</div><!-- leftpanel-profile -->

<div class="leftpanel-userinfo collapse" id="loguserinfo">
    <h5 class="sidebar-title">Address</h5>
    <address>
        Iqbal Town, LAHORE
    </address>
    <h5 class="sidebar-title">Contact</h5>
    <ul class="list-group">
        <li class="list-group-item">
            <label class="pull-left">Email</label>
            <span class="pull-right">odtravels.com</span>
        </li>
        <li class="list-group-item">
            <label class="pull-left">Facebook</label>
            <span class="pull-right">od travels</span>
        </li>
        <li class="list-group-item">
            <label class="pull-left">Mobile</label>
            <span class="pull-right">0334 411 4242</span>
        </li>
        <li class="list-group-item">
            <label class="pull-left">Social</label>
            <div class="social-icons pull-right">
                <a href="#"><i class="fa fa-facebook-official"></i></a>
                {{--<a href="#"><i class="fa fa-twitter"></i></a>--}}
                {{--<a href="#"><i class="fa fa-pinterest"></i></a>--}}
            </div>
        </li>
    </ul>
</div>
<!-- leftpanel-userinfo -->

<ul class="nav nav-tabs nav-justified nav-sidebar">
    <li class="tooltips active" data-toggle="tooltip" title="Main Menu"><a data-toggle="tab" data-target="#mainmenu"><i class="tooltips fa fa-ellipsis-h"></i></a></li>
    {{--<li class="tooltips unread" data-toggle="tooltip" title="Check Mail"><a data-toggle="tab" data-target="#emailmenu"><i class="tooltips fa fa-envelope"></i></a></li>--}}
    {{--<li class="tooltips" data-toggle="tooltip" title="Contacts"><a data-toggle="tab" data-target="#contactmenu"><i class="fa fa-user"></i></a></li>--}}
    {{--<li class="tooltips" data-toggle="tooltip" title="Settings"><a data-toggle="tab" data-target="#settings"><i class="fa fa-cog"></i></a></li>--}}
    <li class="tooltips" data-toggle="tooltip" title="Log Out"><a href="{{ route('admin.auth.logout') }}"><i class="fa fa-sign-out"></i></a></li>
</ul>

<div class="tab-content">

    <!-- ################# MAIN MENU ################### -->
    <div class="tab-pane active" id="mainmenu">
        <h5 class="sidebar-title">Main Menu</h5>
        <ul class="nav nav-pills nav-stacked nav-quirk">
            <div id="jquery-accordion-menu" class="jquery-accordion-menu">
                <ul>
                    <li class="{{ request()->segment(2) == 'dashboard' ? 'active' : '' }}"><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i>Dashboard </a></li>
                    <li class=""><a href=""><i class="fa fa-cog"></i>Home </a><span class="jquery-accordion-menu-label">5 </span>
                        <ul class="submenu">
                            <li class="{{ request()->segment(2) == 'testimonials' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Testimonials</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.testimonials.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.testimonials.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'contacts' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Contact Us</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.contacts.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.contacts.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'home_slider' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Home Slider</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.home_slider.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.home_slider.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'offers' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Offers</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.offers.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.offers.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'news' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>News</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.news.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.news.create') }}">Create</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href=""><i class="fa fa-cog"></i>Tours </a><span class="jquery-accordion-menu-label">4 </span>
                        <ul class="submenu">
                            <li class="{{ request()->segment(2) == 'tours' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i><span>Tours Description </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.tours.index') }}">Show </a></li>
                                    <li><a href="{{ route('admin.tours.create') }}">Create </a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'tours_videos' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i><span>Tours Videos </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.tours_videos.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.tours_videos.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'tours_slider' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i><span>Tours Slider </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.tours_slider.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.tours_slider.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'tours_images' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i><span>Tours Images </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.tours_images.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.tours_images.create') }}">Create</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href=""><i class="fa fa-newspaper-o"></i>Visa </a><span class="jquery-accordion-menu-label">4 </span>
                        <ul class="submenu">
                            <li class="{{ request()->segment(2) == 'visa' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Visa Detail</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.visa.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.visa.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(2) == 'requirments' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>All Requirments</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.requirments.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.requirments.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(3) == 'visa_requirment' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Visa Requirments</span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('admin.visa_requirment.index') }}">Show</a></li>
                                    <li><a href="{{ route('admin.visa_requirment.create') }}">Create</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->segment(3) == 'arrival_countries' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Arrival Countries</span></a>
                                <ul class="submenu">
                                    <li class="{{ request()->segment(3) == 'arrival_countries' ? 'active' : '' }}"><a href="{{ route('admin.arrival_countries.index') }}">Show</a></li>
                                    <li class="{{ request()->segment(3) == 'arrival_countries' ? 'active' : '' }}"><a href="{{ route('admin.arrival_countries.create') }}">Create</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ request()->segment(2) == 'country' ? 'active' : '' }}"><a href=""><i class="fa fa-suitcase"></i>Countries</a>
                        <ul class="submenu">
                            <li><a href="{{ route('admin.country.index') }}">Show</a></li>
                            {{--<li><a href="{{ route('admin.country.create') }}">Create</a></li>--}}
                        </ul>
                    </li>
                    <li class="{{ request()->segment(2) == 'umrah' ? 'active' : '' }}"><a href=""><i class="fa fa-suitcase"></i>Umrah</a>
                        <ul class="submenu">
                            <li><a href="{{ route('admin.umrah.index') }}">Show</a></li>
                            <li><a href="{{ route('admin.umrah.create') }}">Create</a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->segment(2) == 'insaurance' ? 'active' : '' }}"><a href=""><i class="fa fa-suitcase"></i>Insaurance</a>
                        <ul class="submenu">
                            <li class="{{ request()->segment(2) == 'insaurance' ? 'active' : '' }}"><a href="{{ route('admin.insaurance.index') }}">Show</a></li>
                            <li><a href="{{ route('admin.insaurance.create') }}">Create</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            {{--{{ Route::currentRouteName() == 'home' ? 'active' : '' }}--}}
            {{--<li class="{{ request()->segment(2) == 'dashboard' ? 'active' : '' }}"><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>--}}
            {{--<li class=""><a href=""><i class="nav-parent"></i> <span>Home</span></a></li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'testimonials' ? 'active' : '' }}">--}}
            {{--<li class="active">--}}
                {{--<a href="{{ route('admin.testimonials.index') }}"><i class="fa fa-check-square"></i> <span>Testimonials</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.testimonials.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.testimonials.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'contacts' ? 'active' : '' }}">--}}
            {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Contact us</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.contacts.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.contacts.create') }}">Create</a></li>--}}
                    {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'home_slider' ? 'active' : '' }}">--}}
            {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Home Slider</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.home_slider.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.home_slider.create') }}">Create</a></li>--}}
                    {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'offers' ? 'active' : '' }}">--}}
            {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Offers</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.offers.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.offers.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'news' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>News</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.news.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.news.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent">--}}
            {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Tours</span></a>--}}
                {{--<ul class="nav-parent">--}}
                    {{--<li class="nav-parent">--}}
                        {{--<li class="active">--}}
                        {{--<a href=""><i class="fa fa-paragraph"></i> <span>Tour Description</span></a>--}}
                        {{--<ul class="children {{ request()->segment(2) == 'tours' ? 'active' : '' }}">--}}
                              {{--<li class=""><a href="{{ route('admin.tours.index') }}"><i class="fa fa-eye"></i>Show</a></li>--}}
                            {{--<li class="{{ request()->segment(3) == 'tours' ? 'active' : '' }}"><a href="{{ route('admin.tours.create') }}"><i class="fa fa-pencil"></i>Create</a></li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                    {{--</li>--}}
                    {{--<li class="nav-parent {{ request()->segment(2) == 'tours_images' ? 'active' : '' }}">--}}
                        {{--<li class="active">--}}
                        {{--<a href=""><i class="fa fa-check-square"></i> <span>Tours Images</span></a>--}}
                        {{--<ul class="children {{ request()->segment(2) == 'tours_images' ? 'active' : ''}}">--}}
                            {{--<li><a href="{{ route('admin.tours_images.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.tours_images.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="nav-parent {{ request()->segment(2) == 'tours_slider' ? 'active' : '' }}">--}}
                        {{--<li class="active">--}}
                        {{--<a href=""><i class="fa fa-check-square"></i> <span>Tours Slider</span></a>--}}
                        {{--<ul class="children">--}}
                            {{--<li><a href="{{ route('admin.tours_slider.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.tours_slider.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="nav-parent {{ request()->segment(2) == 'tours_videos' ? 'active' : '' }}">--}}
                        {{--<li class="active">--}}
                        {{--<a href=""><i class="fa fa-check-square"></i> <span>Tours Videos</span></a>--}}
                        {{--<ul class="children">--}}
                            {{--<li><a href="{{ route('admin.tours_videos.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.tours_videos.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="nav-parent {{ request()->segment(2) == 'country' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Countries</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.country.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.tours_videos.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'visa' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Visa</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.visa.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.visa.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'requirments' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Requirments</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.requirments.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.requirments.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'visa_requirment' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Visa Requirments</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.visa_requirment.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.visa_requirment.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'umrah' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Umrah</span></a>--}}
                {{--<ul class="children">--}}
                    {{--<li><a href="{{ route('admin.umrah.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.umrah.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'insaurance' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Insaurance</span></a>--}}
                {{--<ul class="children {{ request()->segment(2) == 'dashboard' ? 'active' : '' }}">--}}
                    {{--<li><a href="{{ route('admin.insaurance.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.insaurance.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent {{ request()->segment(2) == 'arrival_countries' ? 'active' : '' }}">--}}
                {{--<li class="active">--}}
                {{--<a href=""><i class="fa fa-check-square"></i> <span>Arrival Countries</span></a>--}}
                {{--<ul class="children {{ request()->segment(2) == 'dashboard' ? 'active' : '' }}">--}}
                    {{--<li><a href="{{ route('admin.arrival_countries.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.arrival_countries.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

        </ul>




    </div>
    <!-- tab-pane -->

</div>



{{--{{dd(request()->segment(2))}}--}}



<!-- new menu -->

{{--<div class="content">--}}
    {{--<div id="jquery-accordion-menu" class="jquery-accordion-menu">--}}
        {{--<div class="jquery-accordion-menu-header">Header </div>--}}
        {{--<ul>--}}
            {{--<li class="{{ request()->segment(2) == 'dashboard' ? 'active' : '' }}"><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i>Dashboard </a></li>--}}
            {{--<li><a href="#"><i class="fa fa-cog"></i>Home </a><span class="jquery-accordion-menu-label">5 </span>--}}
                {{--<ul class="submenu">--}}
                    {{--<li class="{{ request()->segment(2) == 'testimonials' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Testimonials</span></a>--}}
                    {{--<ul class="submenu">--}}
                        {{--<li><a href="{{ route('admin.testimonials.index') }}">Show</a></li>--}}
                        {{--<li><a href="{{ route('admin.testimonials.create') }}">Create</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'contacts' ? 'active' : '' }}"><a href="{{ route('admin.contacts.index') }}"><i class="fa fa-check-square"></i> <span>Contact Us</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.contacts.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.contacts.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'home_slider' ? 'active' : '' }}"><a href="{{ route('admin.home_slider.index') }}"><i class="fa fa-check-square"></i> <span>Home Slider</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.home_slider.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.home_slider.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'offers' ? 'active' : '' }}"><a href="{{ route('admin.offers.index') }}"><i class="fa fa-check-square"></i> <span>Offers</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.offers.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.offers.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'news' ? 'active' : '' }}"><a href="{{ route('admin.news.index') }}"><i class="fa fa-check-square"></i> <span>News</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.news.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.news.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li><a href="#"><i class="fa fa-cog"></i>Tours </a><span class="jquery-accordion-menu-label">4 </span>--}}
                {{--<ul class="submenu">--}}
                    {{--<li class="{{ request()->segment(2) == 'tours' ? 'active' : '' }}"><a href="{{ route('admin.tours.index') }}"><i class="fa fa-check-square"></i><span>Tours Description </span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.tours.index') }}">Show </a></li>--}}
                            {{--<li><a href="{{ route('admin.tours.create') }}">Create </a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'tours_videos' ? 'active' : '' }}"><a href="{{ route('admin.tours_videos.index') }}"><i class="fa fa-check-square"></i><span>Tours Videos </span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.tours_videos.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.tours_videos.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'tours_slider' ? 'active' : '' }}"><a href="{{ route('admin.tours_slider.index') }}"><i class="fa fa-check-square"></i><span>Tours Slider </span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.tours_slider.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.tours_slider.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'tours_images' ? 'active' : '' }}"><a href="{{ route('admin.tours_images.index') }}"><i class="fa fa-check-square"></i><span>Tours Images </span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.tours_images.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.tours_images.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li><a href="#"><i class="fa fa-newspaper-o"></i>Visa </a><span class="jquery-accordion-menu-label">4 </span>--}}
                {{--<ul class="submenu">--}}
                    {{--<li class="{{ request()->segment(2) == 'visa' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Visa Detail</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.visa.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.visa.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'requirments' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>All Requirments</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.requirments.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.requirments.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'visa_requirment' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Visa Requirments</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li><a href="{{ route('admin.visa_requirment.index') }}">Show</a></li>--}}
                            {{--<li><a href="{{ route('admin.visa_requirment.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ request()->segment(2) == 'arrival_countries' ? 'active' : '' }}"><a href=""><i class="fa fa-check-square"></i> <span>Arrival Countries</span></a>--}}
                        {{--<ul class="submenu">--}}
                            {{--<li class="{{ request()->segment(2) == 'arrival_countries' ? 'active' : '' }}"><a href="{{ route('admin.arrival_countries.index') }}">Show</a></li>--}}
                            {{--<li class="{{ request()->segment(2) == 'arrival_countries' ? 'active' : '' }}"><a href="{{ route('admin.arrival_countries.create') }}">Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="{{ request()->segment(2) == 'country' ? 'active' : '' }}"><a href=""><i class="fa fa-suitcase"></i>Countries</a>--}}
                {{--<ul class="submenu">--}}
                    {{--<li><a href="{{ route('admin.country.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.country.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="{{ request()->segment(2) == 'umrah' ? 'active' : '' }}"><a href=""><i class="fa fa-suitcase"></i>Umrah</a>--}}
                {{--<ul class="submenu">--}}
                    {{--<li><a href="{{ route('admin.umrah.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.umrah.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="{{ request()->segment(2) == 'insaurance' ? 'active' : '' }}"><a href=""><i class="fa fa-suitcase"></i>Insaurance</a>--}}
                {{--<ul class="submenu">--}}
                    {{--<li><a href="{{ route('admin.insaurance.index') }}">Show</a></li>--}}
                    {{--<li><a href="{{ route('admin.insaurance.create') }}">Create</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</div>--}}


<!-- /new menu -->


