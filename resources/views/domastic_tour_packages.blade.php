@extends('layouts.app')

@section('content')

<div class="home-1">


    <img src="{{asset('images/domestic.jpg')}}">
    {{--<div class="rainbow-div"><h1 class="rainbow">Tour</h1></div>--}}


</div>

<section id="latest-pkg">
    <div class="container">
        <p>Package tours are excursions or holidays which “package” a variety of services together to make a single “combined” trip. Commonly they combine such things as transport, accommodation and meals. They may also include the provision of a tour guide and/or leader. Tours can be long or short in duration and distance.</p>
        <h1>Latest Packages</h1>
        <div class="row">

            @foreach($tours as $tour)
            <div class="col-md-3">
                <div class="contry-c">
                    <div class="img-container">
                        <a href="{{ route('tour_detail',$tour->id) }}">
                            <img src="{{asset($tour->cover_url)}}" style="height: 159px; width: 253px;"></a>
                        <div class="overlay">
                            <div class="text">
                                <a href="{{ route('tour_detail',$tour->id) }}" target="_self">
                                    {{$tour->heading}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <p><span><b>Stay:</b> {{$tour->stay}}</span></p>
                    <p><b>Hotel:</b> {{$tour->hotel}}</p>
                    <p><b>Ticket:</b> {{$tour->ticket}}</p>
                    <p><b>Visa:</b> {{$tour->visa}}</p>
                    <p><b>Price:</b> {{$tour->price}}</p>
                    <button class="btn btn-primary"><a href="{{ route('domastic_tour_detail') }}"> Further Details</a></button>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@include('layouts.videos')

@endsection