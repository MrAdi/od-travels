@extends('layouts.app')

@section('content')


<div class="home-1">

    <img src="{{asset($visa_detail->banner_url)}}">
    <div class="rainbow-div"><h1 class="rainbow">{{$visa_detail->banner_heading}}</h1></div>

</div>
<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="wrapper center-block">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading " role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fa fa-fax" aria-hidden="true"></i> {{$visa_detail->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img class="img-thumbnail" src="{{asset($visa_detail->country->avatar)}}">
                                        </div>
                                        <div class="col-md-9">
                                            <strong class="accent-color">Description:</strong>
                                            <p>{{$visa_detail->description}}.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--{{dd($visa_detail->country->name)}}--}}
                        {{--<h1 class="cvisa">{{$visa_detail->country->name}}</h1>--}}
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fa fa-folder-open-o" aria-hidden="true"></i>Embassy Requirements
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>

                                            <th>Requirements</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                       {{--{{dd($visa_detail->requirments)}}--}}

                              @foreach($visa_detail->requirments as $requirment)
                                        <tr>
                                            <td>{{$requirment->name}}</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                              @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fa fa-building" aria-hidden="true"></i> Duration Of Stay/ Visa Processing Time
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p><strong>{{$visa_detail->processing_time}}</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sidebar">
                <div class="widget widget-tags">
                    <div class="d-flex">

                        <h4><strong>Embassy List</strong></h4>

                        <!-- Search Panel -->

                        <form>
                            <div class="input-group mb-3">
                                <input id="myInput" type="text" class="form-control" placeholder="search..." aria-label="Email" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <button>
                                            <a href="#"> <i class="fa fa-search" aria-hidden="true"></i></a>
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <!-- Search Panel -->
                    </div>
                    <div class="container">
                        @if(isset($details))
                            <p> The Search results for your query <b> {{ $query }} </b> are :</p>
                            <h2>Countries details</h2>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($details as $countries)
                                    <tr>
                                        <td>{{$countries->name}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>

                    <div class="tagcloud">
                        <div class="visa-row">
                            @foreach($visas as $visa)

                            <div class="visa-flag">
                                <div class="visa-flag-img">
                                    <img alt="" src="{{asset($visa->countries->avatar)}}">
                                </div>
                                <a href="{{$visa->id}}">{{$visa->countries->name}}</a>
                            </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

