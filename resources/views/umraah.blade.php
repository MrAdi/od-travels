@extends('layouts.app')

@section('content')

    <div class="home-1">

        <img src="{{asset('images/umrah.jpg')}}">
        <div class="rainbow-div"><h1 class="rainbow">Umrah</h1></div>




    </div>
    <section id="latest-pkg">
        <div class="container">

            <h1>Latest Packages</h1>
            <div class="row">

                @foreach($umrahs as $umrah)
                <div class="col-md-3">
                    <div class="contry-c">
                        <div class="img-container">
                            <a href="#">
                                <img src="{{asset($umrah->url)}}"></a>
                            <div class="overlay">
                                <div class="text">
                                    <a href="#" target="_self">
                                        {{$umrah->name}}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <p><span>Days:{{$umrah->days}}</span></p>
                        <p>Hotel:{{$umrah->hotel}}</p>
                        <p>Visa:{{$umrah->visa}}</p>
                        <p>Tickets:{{$umrah->ticket}}</p>
                        <p>Rates:{{$umrah->price}}
                        </p>
                        <button class="btn btn-primary"><a href="{{ url('contact_us') }}">book now</a></button>




                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>

@include('layouts.videos')
@endsection