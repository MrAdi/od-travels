<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
////// Frontend page Routes


Route::get('/', 'HomeController@index')->name('home');
Route::get('/visa/{id}', 'HomeController@visa')->name('visa');
Route::get('/visa_category', 'HomeController@visa_category')->name('visa_category');
Route::get('/tour_packages', 'HomeController@tour')->name('tour');
Route::get('/domastic_tour_packages', 'HomeController@domastic_tour')->name('domastic_tour');
Route::get('/about_us', 'HomeController@about_us')->name('about_us');
Route::get('/arrival_countries', 'HomeController@arrival_countries')->name('arrival_countries');
Route::get('/insaurance', 'HomeController@insaurance')->name('insaurance');
Route::get('/umraah', 'HomeController@umraah')->name('umraah');

///////Search

//Route::get('/search');

///////Search

Route::post('/search-tours', 'HomeController@searchtours')->name('search.tour');
//Route::post('/search-countries/{id}', 'HomeController@searchcountries')->name('search.countries');
//Route::get('/tour', 'TourController@item');


///////Mail

//Route::post('/contact', 'ContactController@store');

Route::get('/tour_packages_detail/{id}', 'HomeController@tour_detail')->name('tour_detail');
Route::get('/domastic_tour_packages_detail', 'HomeController@domastic_tour_detail')->name('domastic_tour_detail');
//Route::post('/contact_us/send', 'HomeController@tour_detail');


Route::get('/contact_us', 'SendEmailController@index')->name('contact_us');
Route::post('/contact_us/send', 'SendEmailController@send');

//Route::get('/tour_packages_detail/{id}', 'SendEmailController@index');
//Route::post('/tour_packages_detail/{id}/send', 'SendEmailController@send');


////// Dashboard page Routes

Route::namespace('Admin')->prefix('admin')->as('admin.')->group(function () {

// Controllers Within The "App\Http\Controllers\Admin" Namespace]

    Route::group(['middleware' => ['admin.guest']], function () {
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('show_login');
        Route::post('login', 'Auth\LoginController@login')->name('login');
    });
    Route::group(['middleware' => ['admin.auth']], function () {

        Route::get('/', 'HomeController@index');
        Route::get('/dashboard', 'HomeController@index');
        Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout'
        ]);

////// Registration Routes...
//        Route::get('register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
//        Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@register']);
////
//// Password Reset Routes...
//        Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
//        Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
//        Route::get('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
//        Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\ResetPasswordController@reset']);


////// Dashboard page Routes
        Route::get('/command/migrate', function () {
            /* php artisan migrate */
            \Artisan::call('migrate');
//            dd("Done");
        });

        Route::resource('contacts', 'ContactController');
        Route::resource('country', 'CountryController');
        Route::resource('home_slider', 'HomeSliderController');
        Route::resource('news', 'NewsController');
        Route::resource('offers', 'OfferController');
        Route::resource('requirments', 'RequirmentsController');
        Route::post('/visa_requirment/fetch/{id}', 'VisaRequirmentController@fetch')->name('visa_requirment');
        Route::resource('visa_requirment', 'VisaRequirmentController');
        Route::resource('testimonials', 'TestimonialController');
        Route::resource('tours', 'TourController');
        Route::resource('tours_images', 'TourImagesController');
        Route::resource('tours_slider', 'TourSliderController');
        Route::resource('tours_videos', 'TourVideoController');
        Route::resource('visa', 'VisaController');
        Route::resource('arrival_countries', 'ArrivalCountriesController');
        Route::resource('insaurance', 'InsauranceController');
        Route::resource('umrah', 'UmrahController');
        Route::resource('domastic', 'DomasticController');


    });
});




